from test_framework._support import *
from test_framework._test_params import *


@davis_input
@output_hash([
    "73abc7d09ac9474457048ac8fd746ba27e1768e90aa9aa1ae35991c8d0186cf8",
    "20ef77a919da3a014250077564cb5eb3cdf820395ee7177bec20b7c658b0abf4"  # OpenCV 4.8
])
@calibration_required
def test_12_01_UndistortEvents(self):
    calib_file = test_params["calibration_file"]

    # Check condition to run
    check_run(self)

    # Check calibration file
    self.assertEqual(os.path.exists(calib_file), True, 'Check if calibration file exists:{}'.format(calib_file))

    module_parameters = [["fitMorePixels", "0.2"], ["calibrationFile", calib_file]]
    module = dv_module("dv_undistort", [["events", "input[events]"]], ["undistortedEvents"],
                       config_options=module_parameters)
    test_modules_with_io(self, [module])
