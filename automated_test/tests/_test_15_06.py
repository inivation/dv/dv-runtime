from test_framework._support import *
from test_framework._test_params import *


@dvxplorer_performance_input
@test_performance
def test_15_06_perf_video_output(self):
    output_file = create_output_file_name_mkv()
    module_parameters = [["fileName", output_file], ["codec", "FFV1_[ffv1]"]]
    modules = [
        dv_module("dv_accumulator", [["events", "input[events]"]], ["frames"]),
        dv_module("dv_video_output", [["frames", "dv_accumulator[frames]"]], config_options=module_parameters)
    ]

    input_params = [["seekStart", "0"], ["seekEnd", 2000000], ["logLevel", "ERROR"]]
    test_modules_with_input(self, modules, input_parameters=input_params)
