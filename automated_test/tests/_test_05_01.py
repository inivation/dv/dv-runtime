from test_framework._support import *
from test_framework._test_params import *


@dvxplorer_input
# Exponential decay is calculated subtly differently on different platforms.
# Therefore we need to provide different expected results for each platform.
@output_hash([
    "40ef3d5f79c145abdf87030c21750427f043edb8faa66dbe8389ae2394b7599f",  # MacOS Intel
    "3a1e7da1193c042f5801a35c72bda30bab8500fbf75d0a277851513ea25b1f9f",  # MacOS ARM
    "783407a7c63fc8f262eb8de10bd000a58030ad6e4b0dcde12438bc7e0e38ce91",  # Windows
    "1e6c3c2e8d61158994d62636abdd547cd99544a3352dc0e76108bb87c9b58f37"  # Linux
])
def test_05_01_accumulatorPlayFile(self):
    module = dv_module("dv_accumulator", [["events", "input[events]"]], ["frames"])
    test_modules_with_io(self, [module])
