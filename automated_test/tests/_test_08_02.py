from test_framework._support import *
from test_framework._test_params import *


@davis_input
@output_hash("da808a27aba01dbd73c7a4374444f2089eab3e2da51133402c3e3130804e4251")
def test_08_02_FlipRotateFramesHVD(self):
    module_parameters = [["degreeOfRotation", "90"], ["frameFlipVertically", "true"], ["frameFlipHorizontally", "true"]]
    module = dv_module("dv_flip_rotate", [["frames", "input[frames]"]], ["frames"], config_options=module_parameters)
    test_modules_with_io(self, [module])
