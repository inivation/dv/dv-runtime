import platform
import time
from test_framework._dv_interface import start_runtime, runtime_out_print
from test_framework._support import *
from test_framework._test_params import *


@standard_params
def test_01_terminate(self):
    # Test name
    test_name = test_params["name"]

    # Check Run
    check_run(self)

    # Start time
    start = time.time()

    log_file = open(os.environ['DV_TEST_OUTPUT_DIR'] + os.path.sep + test_params["name"] + "_log.txt", "w+")
    runtime = start_runtime(log_file)

    time.sleep(5)

    runtime.terminate()

    try:
        runtime.wait(30)
    finally:
        runtime_output = runtime_out_print(runtime)
        log_file.close()

    # wait() will have set returncode properly, OR raise a TimeoutException.
    print("\truntime closed with return code = " + str(runtime.returncode))

    if (platform.system().upper().find("MINGW") != -1) or (platform.system() == "Windows"):
        self.assertEqual(runtime.returncode, 1)
    else:
        self.assertEqual(runtime.returncode, 0)

    # Sending data to database
    upload_results_to_SQL_DB(total_test_time=round((time.time() - start) * 1000), runtime=runtime_output)
