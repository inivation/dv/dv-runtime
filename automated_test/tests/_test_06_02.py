from test_framework._support import *
from test_framework._test_params import *


@dvxplorer_input
@output_hash("7b0ff0366a994536378492144cca650eebd8b4f4e1185978ca58965748cb4046")
def test_06_02_dvsnoisefilterHotPixel(self):
    module_parameters = [["hotPixelLearn", "true"], ["hotPixelEnable", "true"]]
    module = dv_module("dv_dvsnoisefilter", [["events", "input[events]"]], ["events"], config_options=module_parameters)
    test_modules_with_io(self, [module])
