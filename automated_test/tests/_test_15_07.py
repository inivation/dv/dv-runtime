from test_framework._support import *
from test_framework._test_params import *


@dvxplorer_performance_input
@test_performance
def test_15_07_perf_csv_output(self):
    output_file = create_output_file_name_csv()
    module_parameters = [["fileName", output_file]]
    input_params = [["seekStart", "0"], ["seekEnd", "2000000"], ["logLevel", "ERROR"]]
    module = [dv_module("dv_export_csv", [["events", "input[events]"]], config_options=module_parameters)]
    test_modules_with_input(self, module, input_parameters=input_params)
