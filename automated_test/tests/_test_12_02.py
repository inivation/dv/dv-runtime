from test_framework._support import *
from test_framework._test_params import *


@davis_input
@output_hash([
    "b68f45ca85977baf53e0fc477036417c6a70478c14b7c5331e84cf8d79a4fb20",
    "edd665df40a440792a5e59073be9f47f1ea46c14747e0206eb98c0ab4d225567"  # OpenCV 4.8
])
@calibration_required
def test_12_02_UndistortFrames(self):
    calib_file = test_params["calibration_file"]

    # Check condition to run
    check_run(self)

    # Check calibration file
    self.assertEqual(os.path.exists(calib_file), True, 'Check if calibration file exists:{}'.format(calib_file))

    module_parameters = [["fitMorePixels", "0.2"], ["calibrationFile", calib_file]]
    module = dv_module("dv_undistort", [["frames", "input[frames]"]], ["undistortedFrames"],
                       config_options=module_parameters)
    test_modules_with_io(self, [module])
