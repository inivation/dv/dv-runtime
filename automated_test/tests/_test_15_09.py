from test_framework._support import *
from test_framework._test_params import *


@dvxplorer_performance_input
@test_performance
def test_15_09_perf_decimation(self):
    module_parameters = [["percentage", "50"]]
    module = [dv_module("dv_decimation", [["events", "input[events]"]], ["events"], config_options=module_parameters)]
    input_params = [["seekStart", "0"], ["seekEnd", 2000000], ["logLevel", "ERROR"]]
    test_modules_with_input(self, module, input_parameters=input_params)
