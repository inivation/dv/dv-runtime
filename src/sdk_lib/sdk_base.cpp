#include "../log.hpp"
#include "../main.hpp"

#include <boost/core/demangle.hpp>

#include <typeinfo>

const dv::SDKLibFunctionPointers *dv::glLibFuncPtr = nullptr;

void dv::SDKLibInit(const dv::SDKLibFunctionPointers *setLibFuncPtr) {
	dv::glLibFuncPtr = setLibFuncPtr;
}

struct dvType dvTypeSystemGetInfoByIdentifier(const char *tIdentifier) {
	try {
		return (dv::glLibFuncPtr->getTypeInfoCharString(tIdentifier, nullptr));
	}
	catch (const std::exception &ex) {
		dv::Log(dv::logLevel::ERROR, "dvTypeSystemGetInfoByIdentifier(): '{:s} :: {:s}'.",
			boost::core::demangle(typeid(ex).name()), ex.what());

		// Return empty placeholder.
		return (dv::glLibFuncPtr->getTypeInfoIntegerID(dv::Types::nullId, nullptr));
	}
}

struct dvType dvTypeSystemGetInfoByID(uint32_t tId) {
	try {
		return (dv::glLibFuncPtr->getTypeInfoIntegerID(tId, nullptr));
	}
	catch (const std::exception &ex) {
		dv::Log(dv::logLevel::ERROR, "dvTypeSystemGetInfoByID(): '{:s} :: {:s}'.",
			boost::core::demangle(typeid(ex).name()), ex.what());

		// Return empty placeholder.
		return (dv::glLibFuncPtr->getTypeInfoIntegerID(dv::Types::nullId, nullptr));
	}
}

void dvLog(enum dvLogLevel level, const char *message) {
	dv::glLibFuncPtr->log(static_cast<dv::logLevel>(level), message);
}
