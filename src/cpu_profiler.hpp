#ifndef CPU_PROFILER_HPP
#define CPU_PROFILER_HPP

#include "../include/dv-sdk/utils.h"

#include <boost/filesystem.hpp>

#include <gperftools/profiler.h>

static inline bool GCPUProfilerStart(const std::string &outputDirectory, const std::string &outputFilePrefix) {
	// Verify output directory path.
	boost::filesystem::path dirPath{outputDirectory};
	dirPath = boost::filesystem::absolute(dirPath).make_preferred();

	if (dirPath.empty() || !boost::filesystem::exists(dirPath) || !boost::filesystem::is_directory(dirPath)) {
		return (false);
	}

	// Get current time for suffix part.
	const auto fileName = fmt::format("{:s}-{:%Y_%m_%d_%H_%M_%S}.prof", outputFilePrefix,
		fmt::localtime(std::chrono::system_clock::to_time_t(std::chrono::system_clock::now())));

	// Build full path.
	const auto filePath = dirPath / fileName;

	// Enable CPU profiler.
	ProfilerStart(filePath.string().c_str());

	return (true);
}

static inline void GCPUProfilerStop() {
	ProfilerFlush();
	ProfilerStop();
}

#endif // CPU_PROFILER_HPP
