#ifndef CONFIG_HPP_
#define CONFIG_HPP_

#include "../include/dv-sdk/utils.h"

namespace dv {

static constexpr const char *DEFAULT_LOG_FILE = ".dv-runtime.log";
static constexpr const char *DEFAULT_ADDRESS  = "127.0.0.1";
static constexpr uint16_t DEFAULT_PORT        = 4040;

// Create configuration storage, initialize it with content from the
// configuration file, and apply eventual CLI overrides.
void ConfigInit(int argc, char *argv[]);

} // namespace dv

#endif /* CONFIG_HPP_ */
