# Copyright 2021 iniVation AG
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit eutils cmake

DESCRIPTION="C++ event-based processing framework for neuromorphic cameras, targeting embedded and desktop systems."
HOMEPAGE="https://gitlab.com/inivation/dv/${PN}/"

SRC_URI="https://release.inivation.com/runtime/${P}.tar.gz"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="amd64 x86 arm64 arm"
IUSE="debug"

RDEPEND="acct-group/dv-runtime
	acct-user/dv-runtime
	>=dev-libs/dv-processing-1.7.2
	>=dev-libs/boost-1.78.0
	>=media-libs/opencv-4.5.5
	>=dev-libs/libcaer-3.3.14
	>=app-arch/lz4-1.9.0
	>=app-arch/zstd-1.5.0
	>=dev-libs/libfmt-8.1.1
	dev-libs/openssl
	>=dev-util/google-perftools-2.9
	>=media-video/aravis-0.8.22[usb]
	>=media-video/ffmpeg-4.4.2
	>=media-libs/libsdl2-2.24.2"

DEPEND="${RDEPEND}
	virtual/pkgconfig
	>=dev-util/cmake-3.22.0"

src_configure() {
	local mycmakeargs=(
		-DDVR_ENABLE_TCMALLOC=ON
		-DDVR_ENABLE_PROFILER=ON
	)

	cmake_src_configure
}

src_install() {
	cmake_src_install

	newinitd service/${PN}.openrc ${PN}
}
