%global __cmake_in_source_build 1
%global debug_package %{nil}

Summary: C++ event-based processing framework for neuromorphic cameras
Name:    dv-runtime
Version: VERSION_REPLACE
Release: 0%{?dist}
License: ASL 2.0
URL:     https://gitlab.com/inivation/dv/dv-runtime/
Vendor:  iniVation AG

Source0: https://release.inivation.com/runtime/%{name}-%{version}.tar.gz

BuildRequires: gcc >= 10.0, gcc-c++ >= 10.0, systemd-rpm-macros, cmake >= 3.22, pkgconfig >= 0.29.0, libcaer-devel >= 3.3.14, dv-processing >= 1.7.2, boost-devel >= 1.76, openssl-devel, opencv-devel >= 4.5.0, gperftools-devel >= 2.9, SDL2-devel, lz4-devel, libzstd-devel, fmt-devel >= 8.1.1, aravis-devel >= 0.8.22, ffmpeg-free-devel
Requires: dv-processing-utils >= 1.7.2, libcaer >= 3.3.14, boost >= 1.76, openssl, opencv >= 4.5.0, gperftools >= 2.9, SDL2, lz4-libs, libzstd, fmt >= 8.1.1, aravis >= 0.8.22, ffmpeg-free

%description
C++ event-based processing framework for neuromorphic cameras,
targeting embedded and desktop systems.

%package devel
Summary: C++ event-based processing framework for neuromorphic cameras (development files)
Requires: %{name}%{?_isa} = %{version}-%{release}, cmake >= 3.22, pkgconfig >= 0.29.0, libcaer-devel >= 3.3.14, dv-processing >= 1.7.2, boost-devel >= 1.76, openssl-devel, opencv-devel >= 4.5.0, fmt-devel >= 8.1.1

%description devel
Development files for dv-runtime, such as headers, pkg-config files, etc.,
enabling users to write their own processing modules.

%prep
%autosetup

%build
%cmake -UBUILD_SHARED_LIBS -DDVR_ENABLE_TCMALLOC=ON -DDVR_ENABLE_PROFILER=ON
%cmake_build

%install
export QA_RPATHS=$(( 0x0001|0x0010|0x0002 ))
%cmake_install

%files
%{_bindir}/dv-runtime
%{_bindir}/dv-control
%{_libdir}/libdvsdk.so.*
%{_datarootdir}/dv/modules/
%{_unitdir}/dv-runtime.service
%{_sysusersdir}/dv-runtime.sysusers.conf

%files devel
%{_includedir}/dv-sdk/
%{_datarootdir}/dv/dv-modules.cmake
%{_libdir}/libdvsdk.so
%{_libdir}/pkgconfig/
%{_libdir}/cmake/dv/

%changelog
* Mon Apr 20 2020 iniVation AG <support@inivation.com> - VERSION_REPLACE
- See ChangeLog file in source or GitLab.
