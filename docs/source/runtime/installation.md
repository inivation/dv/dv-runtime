# Installation

Installation of `dv-runtime` is only provided on [{fa}`apple` Mac](#macos) and [{fa}`linux` Linux](#linux) systems.

## {fa}`apple` MacOS

The easiest way to install `dv-runtime` is via [Homebrew](https://brew.sh/). We provide a
[Homebrew tap](https://gitlab.com/inivation/homebrew-inivation/) for macOS. Install it with:

```bash
brew tap inivation/inivation
brew install libcaer --with-libserialport --with-opencv
brew install dv-runtime
```

## {fa}`linux` Linux

### Ubuntu Linux

We provide a [PPA repository](https://launchpad.net/~inivation-ppa/+archive/ubuntu/inivation) for Focal (20.04 LTS) and
Jammy (22.04 LTS) on the x86_64, arm64 and armhf architectures.

```bash
sudo add-apt-repository ppa:inivation-ppa/inivation
sudo apt-get update
sudo apt-get install dv-runtime
```

#### Ubuntu Bionic

We provide a [separate PPA repository](https://launchpad.net/~inivation-ppa/+archive/ubuntu/inivation-bionic) for Bionic
(18.04 LTS) on the x86_64, x86, arm64 and armhf architectures.

```bash
sudo add-apt-repository ppa:ubuntu-toolchain-r/test
sudo add-apt-repository ppa:inivation-ppa/inivation-bionic
sudo apt-get update
sudo apt-get install dv-runtime
```

### Fedora Linux

We provide a [COPR repository](https://copr.fedorainfracloud.org/coprs/inivation/inivation/) for Fedora 34, 35, 36 and
rawhide on the x86_64, arm64 and armhf architectures.

```bash
sudo dnf copr enable inivation/inivation
sudo dnf install dv-runtime
```

### Arch Linux

You can find `dv-runtime` as [a package in the AUR repository](https://aur.archlinux.org/packages/dv-runtime).

### Gentoo Linux

You can find `dv-runtime` as a valid Gentoo ebuild repository available
[here](https://gitlab.com/inivation/gentoo-inivation/) over Git. The package to install is 'dev-util/dv-runtime'.

### Embedded Devices

On most embedded devices, you can follow the [Ubuntu Linux instructions](#ubuntu-linux).

To start a `dv-runtime` instance, you might have to set the following environment variable: `UNW_ARM_UNWIND_METHOD=4`.

#### Nvidia Jetson

The Nvidia Jetson embedded boards are supported via [our Ubuntu Bionic packages](#ubuntu-bionic). The
[Linux for Tegra (L4T)](https://developer.nvidia.com/embedded/linux-tegra) distribution is based on Ubuntu Bionic (18.04
LTS), so the same installation instructions apply. One thing to note: Nvidia provides an incompatible version of
OpenCV4, so you'll first have to uninstall their version that's pre-installed and then install the one from our PPA
repository.

#### Raspberry Pi

Raspberry Pi is supported on the Ubuntu operating system. Ubuntu is an
[officially supported OS for Raspberry Pi](https://ubuntu.com/download/raspberry-pi). Please use
[our Ubuntu packages](#ubuntu-linux).
