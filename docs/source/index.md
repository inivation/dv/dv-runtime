# Welcome to DV-SDK and DV-Runtime documentation!

## DV-SDK

```{toctree}
---
caption: DV-SDK
hidden:
---
/sdk/installation
/sdk/dv-modules/index
```

`dv-sdk` is the library we provide to extend the provided functionalities of the
[DV](https://docs.inivation.com/master/software/dv/index.html) software or integrate other algorithms into it. It is
used to create new modules that would implement new processing features or integrate existing algorithms.

In this documentation we explain how to:

- [Install the dv-sdk library](/sdk/installation.md)
- [Understand and Create DV Modules](/sdk/dv-modules/index.md)

## DV-Runtime

```{toctree}
---
caption: DV-Runtime
hidden:
---
/runtime/installation
/runtime/usage/index
```

`dv-runtime` is our provided [Runtime System](https://en.wikipedia.org/wiki/Runtime_system). It is the environment that
will host the modules and take care of the threads, the memory allocation and data exchange between the modules. It is
made to be used via [DV](https://docs.inivation.com/master/software/dv/index.html), but it can also be used on its own.

In this documentation we explain how to:

- [Install dv-runtime](/runtime/installation.md)
- [Use dv-runtime](/runtime/usage/index.md)

```{toctree}
---
caption: API
hidden:
---
/api
```
