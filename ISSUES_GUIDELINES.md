# dv-runtime Issues Guidelines

## Important Points

- No work without an issue!
- No Merge Request without corresponding issue!
- Branches follow issue naming, eg. 'ISSUE_NUMBER-my-descriptive-branch-name'.
- Major work should be split into multiple issues as makes sense, with a tracker issue (title must contain 'Tracker: ')
  that is blocked by the sub-issues. Also possibly group issues together with milestones.
- Use the issue and merge request templates, fill them out as completely as possible.
- Try to break longer issues into tasks, and use the '\* \[ \] xyz' syntax to make a list of task checkboxes.
- Label issues 'Short Term' when they should enter planning for the next 2-3 months, 'Long Term' issues are
  later/currently unplanned.
- Ensure 'Weight' is set to communicate issue impact and importance, from 1 (lowest) to 10 (highest).
- Set and keep time estimates and time spent updated, use the '/estimate' and '/spend' commands in Gitlab comments.
- Keep the StartDate and DueDate in the description updated, do not use the 'Due Date' in the right-side Gitlab menu (it
  duplicates information).
- Once you start working on a bug, assign it to yourself, if it isn't already.
- Ensure all applicable labels are set for an issue. Especially one of 'Short Term' or 'Long Term' must always be set
  for planning.
- Update the issues you're working on at least weekly, possibly daily, with progress and spent time.
- If the issue requires changes to multiple products, eg. runtime and GUI, or runtime and Docs, create an issue in the
  other products too, and use the linked issues support to reference them ('group/project#issue_number' syntax).

## Working on an Issue

When taking on an issue, make sure its up-to-date:

- Description current and clear?
- Weight/Time Estimate/StartDate/DueDate all set.
- Assign to yourself!
- Move from milestone 'next' into expected release milestone, if applicable.
