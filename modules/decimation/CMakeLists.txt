ADD_LIBRARY(decimation SHARED decimation.cpp)

SET_TARGET_PROPERTIES(decimation PROPERTIES PREFIX "dv_")

TARGET_LINK_LIBRARIES(decimation PRIVATE dv::sdk)

INSTALL(TARGETS decimation DESTINATION ${DV_MODULES_DIR})
