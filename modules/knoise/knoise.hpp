#include "../../include/dv-sdk/module.hpp"

struct k_mem_cell {
	int64_t timestamp;
	bool passed;
	bool polarity;
	uint16_t otherAddr;
};

class Knoise : public dv::ModuleBase {
public:
	static const char *initDescription();

	static void initInputs(dv::InputDefinitionList &in);

	static void initOutputs(dv::OutputDefinitionList &out);

	static void initConfigOptions(dv::RuntimeConfig &config);

	Knoise();

	void run() override;

	void configUpdate() override;

private:
	std::vector<k_mem_cell> xCols;
	std::vector<k_mem_cell> yRows;
	int32_t deltaT;
	int32_t sizeX;
	int32_t sizeY;
	size_t supporters;
};

registerModuleClass(Knoise)
