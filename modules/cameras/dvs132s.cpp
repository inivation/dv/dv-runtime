#include "../../include/dv-sdk/data/event.hpp"
#include "../../include/dv-sdk/data/imu.hpp"
#include "../../include/dv-sdk/data/trigger.hpp"
#include "../../include/dv-sdk/module.hpp"

#include "../../src/log.hpp"
#include "aedat4_convert.hpp"

#include <libcaercpp/devices/device_discover.hpp>
#include <libcaercpp/devices/dvs132s.hpp>

#include <chrono>

void *dvModuleGetHooks(enum dvModuleHooks hook) {
	// Support only device discovery.
	if (hook != DV_HOOK_DEVICE_DISCOVERY) {
		return nullptr;
	}

	// Get current device status via libcaer.
	// Suppress logging of libusb errors.
	std::vector<struct caer_device_discovery_result> devices;

	try {
		libcaer::log::disable(true);

		devices = libcaer::devices::discover::device(CAER_DEVICE_DVS132S);

		libcaer::log::disable(false);
	}
	catch (const std::exception &ex) {
		dv::Log(dv::logLevel::ERROR, "Device Discovery (DVS132S): '{:s} :: {:s}'.",
			boost::core::demangle(typeid(ex).name()), ex.what());
		return nullptr;
	}

	// Add devices to config tree.
	auto devicesNode = dv::Cfg::GLOBAL.getNode("/system/_devices/");

	for (const auto &dev : devices) {
		const struct caer_dvs132s_info *info = &dev.deviceInfo.dvs132sInfo;

		const auto nodeName = fmt::format(FMT_STRING("dvs132s_{:d}-{:d}/"), static_cast<int>(info->deviceUSBBusNumber),
			static_cast<int>(info->deviceUSBDeviceAddress));

		auto devNode = devicesNode.getRelativeNode(nodeName);

		devNode.create<dv::CfgType::STRING>("OpenWithModule", "dv_dvs132s", {1, 32},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Open device with specified module.");

		devNode.create<dv::CfgType::INT>("USBBusNumber", info->deviceUSBBusNumber, {0, 255},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "USB bus number.");
		devNode.create<dv::CfgType::INT>("USBDeviceAddress", info->deviceUSBDeviceAddress, {0, 255},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "USB device address.");

		if (!dev.deviceErrorOpen) {
			devNode.create<dv::CfgType::STRING>("SerialNumber", info->deviceSerialNumber, {0, 8},
				dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "USB device serial number.");
			devNode.create<dv::CfgType::INT>("FirmwareVersion", info->firmwareVersion, {0, INT16_MAX},
				dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Version of device firmware.");

			if (!dev.deviceErrorVersion) {
				devNode.create<dv::CfgType::INT>("LogicVersion", info->logicVersion, {0, INT16_MAX},
					dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Version of FPGA logic.");
				devNode.create<dv::CfgType::BOOL>("DeviceIsMaster", info->deviceIsMaster, {},
					dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Device is timestamp master.");

				devNode.create<dv::CfgType::INT>("DVSSizeX", info->dvsSizeX, {0, INT16_MAX},
					dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "DVS X axis resolution.");
				devNode.create<dv::CfgType::INT>("DVSSizeY", info->dvsSizeY, {0, INT16_MAX},
					dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "DVS Y axis resolution.");
			}
		}
	}

	// Nothing to return, no further steps needed.
	return nullptr;
}

class dvs132s : public dv::ModuleBase {
private:
	libcaer::devices::dvs132s device;

public:
	static void initOutputs(dv::OutputDefinitionList &out) {
		out.addEventOutput("events");
		out.addTriggerOutput("triggers");
		out.addIMUOutput("imu");
	}

	static const char *initDescription() {
		return ("iniVation DVS132S camera support.");
	}

	static void initConfigOptions(dv::RuntimeConfig &config) {
		config.add("busNumber", dv::ConfigOption::intOption("USB bus number restriction.", 0, 0, UINT8_MAX));
		config.add("devAddress", dv::ConfigOption::intOption("USB device address restriction.", 0, 0, UINT8_MAX));
		config.add("serialNumber", dv::ConfigOption::stringOption("USB serial number restriction.", ""));

		biasConfigCreate(config);
		multiplexerConfigCreate(config);
		dvsConfigCreate(config);
		imuConfigCreate(config);
		externalInputConfigCreate(config);
		usbConfigCreate(config);
		systemConfigCreate(config);
	}

	dvs132s() :
		device(0, static_cast<uint8_t>(config.getInt("busNumber")), static_cast<uint8_t>(config.getInt("devAddress")),
			config.getString("serialNumber")) {
		// Initialize per-device log-level to module log-level.
		config.setString("logLevel", "WARNING");
		device.configSet(CAER_HOST_CONFIG_LOG, CAER_HOST_CONFIG_LOG_LEVEL,
			static_cast<uint32_t>(dv::LoggerInternal::logLevelNameToInteger(config.getString("logLevel"))));

		auto devInfo = device.infoGet();

		// Generate source string for output modules.
		auto sourceString = std::string("DVS132S_") + devInfo.deviceSerialNumber;

		// Setup outputs.
		outputs.getEventOutput("events").setup(device.infoGet().dvsSizeX, device.infoGet().dvsSizeY, sourceString);
		outputs.getTriggerOutput("triggers").setup(sourceString);
		outputs.getIMUOutput("imu").setup(sourceString);

		auto sourceInfoNode = moduleNode.getRelativeNode("sourceInfo/");

		sourceInfoNode.create<dv::CfgType::STRING>("serialNumber", devInfo.deviceSerialNumber, {0, 8},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Device serial number.");
		sourceInfoNode.create<dv::CfgType::INT>("usbBusNumber", devInfo.deviceUSBBusNumber, {0, 255},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Device USB bus number.");
		sourceInfoNode.create<dv::CfgType::INT>("usbDeviceAddress", devInfo.deviceUSBDeviceAddress, {0, 255},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Device USB device address.");

		sourceInfoNode.create<dv::CfgType::INT>("firmwareVersion", devInfo.firmwareVersion,
			{devInfo.firmwareVersion, devInfo.firmwareVersion}, dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT,
			"Device USB firmware version.");
		sourceInfoNode.create<dv::CfgType::INT>("logicVersion", devInfo.logicVersion,
			{devInfo.logicVersion, devInfo.logicVersion}, dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT,
			"Device FPGA logic version.");
		sourceInfoNode.create<dv::CfgType::INT>("chipID", devInfo.chipID, {devInfo.chipID, devInfo.chipID},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Device chip identification number.");

		// Extra features.
		sourceInfoNode.create<dv::CfgType::BOOL>("muxHasStatistics", devInfo.muxHasStatistics, {},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT,
			"Device supports FPGA Multiplexer statistics (USB event drops).");
		sourceInfoNode.create<dv::CfgType::BOOL>("dvsHasStatistics", devInfo.dvsHasStatistics, {},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Device supports FPGA DVS statistics.");
		sourceInfoNode.create<dv::CfgType::BOOL>("extInputHasGenerator", devInfo.extInputHasGenerator, {},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT,
			"Device supports generating pulses on output signal connector.");

		sourceInfoNode.create<dv::CfgType::BOOL>("deviceIsMaster", devInfo.deviceIsMaster, {},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT,
			"Timestamp synchronization support: device master status.");

		sourceInfoNode.create<dv::CfgType::STRING>("source", sourceString,
			{static_cast<int32_t>(sourceString.length()), static_cast<int32_t>(sourceString.length())},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Device source information.");

		try {
			// Ensure good defaults for data acquisition settings.
			// No blocking behavior due to mainloop notification, and no auto-start of
			// all producers to ensure cAER settings are respected.
			device.configSet(CAER_HOST_CONFIG_DATAEXCHANGE, CAER_HOST_CONFIG_DATAEXCHANGE_BLOCKING, true);
			device.configSet(CAER_HOST_CONFIG_DATAEXCHANGE, CAER_HOST_CONFIG_DATAEXCHANGE_START_PRODUCERS, false);
			device.configSet(CAER_HOST_CONFIG_DATAEXCHANGE, CAER_HOST_CONFIG_DATAEXCHANGE_STOP_PRODUCERS, true);

			// Create default device-dependant settings.
			multiplexerConfigCreateDynamic(&devInfo);
			dvsConfigCreateDynamic(&devInfo);
			externalInputConfigCreateDynamic(&devInfo);

			// Set timestamp offset for real-time timestamps. DataStart() will
			// reset the device-side timestamp.
			int64_t tsNowOffset = std::chrono::duration_cast<std::chrono::microseconds>(
				std::chrono::system_clock::now().time_since_epoch())
									  .count();

			sourceInfoNode.create<dv::CfgType::LONG>("tsOffset", tsNowOffset, {0, INT64_MAX},
				dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT,
				"Time offset of data stream starting point to Unix time in µs.");

			moduleNode.getRelativeNode("outputs/events/info/")
				.create<dv::CfgType::LONG>("tsOffset", tsNowOffset, {0, INT64_MAX},
					dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT,
					"Time offset of data stream starting point to Unix time in µs.");

			moduleNode.getRelativeNode("outputs/triggers/info/")
				.create<dv::CfgType::LONG>("tsOffset", tsNowOffset, {0, INT64_MAX},
					dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT,
					"Time offset of data stream starting point to Unix time in µs.");

			moduleNode.getRelativeNode("outputs/imu/info/")
				.create<dv::CfgType::LONG>("tsOffset", tsNowOffset, {0, INT64_MAX},
					dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT,
					"Time offset of data stream starting point to Unix time in µs.");

			// Start data acquisition.
			device.dataStart(nullptr, nullptr, nullptr, &moduleShutdownNotify, moduleData->moduleNode);

			// Send all configuration to the device.
			sendDefaultConfiguration(&devInfo);
		}
		catch (const std::exception &ex) {
			// Remove statistics read modifiers.
			if (moduleNode.existsRelativeNode("statistics/")) {
				moduleNode.getRelativeNode("statistics/").attributeUpdaterRemoveAll();
			}

			// Clear sourceInfo node.
			auto sourceInfoNode = moduleNode.getRelativeNode("sourceInfo/");
			sourceInfoNode.removeAllAttributes();

			throw;
		}

		// Add config listeners last, to avoid having them dangling if Init doesn't succeed.
		moduleNode.getRelativeNode("bias/").addAttributeListener(&device, &biasConfigListener);

		moduleNode.getRelativeNode("multiplexer/").addAttributeListener(&device, &multiplexerConfigListener);

		moduleNode.getRelativeNode("dvs/").addAttributeListener(&device, &dvsConfigListener);

		moduleNode.getRelativeNode("imu/").addAttributeListener(&device, &imuConfigListener);

		moduleNode.getRelativeNode("externalInput/").addAttributeListener(&device, &externalInputConfigListener);

		moduleNode.getRelativeNode("usb/").addAttributeListener(&device, &usbConfigListener);

		moduleNode.getRelativeNode("system/").addAttributeListener(&device, &systemConfigListener);

		moduleNode.addAttributeListener(&device, &logLevelListener);
	}

	~dvs132s() override {
		// Remove listener, which can reference invalid memory in userData.
		moduleNode.getRelativeNode("bias/").removeAttributeListener(&device, &biasConfigListener);

		moduleNode.getRelativeNode("multiplexer/").removeAttributeListener(&device, &multiplexerConfigListener);

		moduleNode.getRelativeNode("dvs/").removeAttributeListener(&device, &dvsConfigListener);

		moduleNode.getRelativeNode("imu/").removeAttributeListener(&device, &imuConfigListener);

		moduleNode.getRelativeNode("externalInput/").removeAttributeListener(&device, &externalInputConfigListener);

		moduleNode.getRelativeNode("usb/").removeAttributeListener(&device, &usbConfigListener);

		moduleNode.getRelativeNode("system/").removeAttributeListener(&device, &systemConfigListener);

		moduleNode.removeAttributeListener(&device, &logLevelListener);

		// Stop data acquisition.
		device.dataStop();

		// Remove statistics read modifiers.
		if (moduleNode.existsRelativeNode("statistics/")) {
			moduleNode.getRelativeNode("statistics/").attributeUpdaterRemoveAll();
		}

		// Clear sourceInfo node.
		auto sourceInfoNode = moduleNode.getRelativeNode("sourceInfo/");
		sourceInfoNode.removeAllAttributes();
	}

	void run() override {
		auto data = device.dataGet();

		if (!data || data->empty()) {
			return;
		}

		if (data->getEventPacket(SPECIAL_EVENT)) {
			std::shared_ptr<const libcaer::events::SpecialEventPacket> special
				= std::static_pointer_cast<libcaer::events::SpecialEventPacket>(data->getEventPacket(SPECIAL_EVENT));

			if (special->getEventNumber() == 1 && (*special)[0].getType() == TIMESTAMP_RESET) {
				// Update master/slave information.
				auto devInfo = device.infoGet();

				auto sourceInfoNode = moduleNode.getRelativeNode("sourceInfo/");
				sourceInfoNode.updateReadOnly<dv::CfgType::BOOL>("deviceIsMaster", devInfo.deviceIsMaster);

				// Reset real-time timestamp offset.
				int64_t tsNowOffset = std::chrono::duration_cast<std::chrono::microseconds>(
					std::chrono::system_clock::now().time_since_epoch())
										  .count();

				sourceInfoNode.updateReadOnly<dv::CfgType::LONG>("tsOffset", tsNowOffset);

				moduleNode.getRelativeNode("outputs/events/info/")
					.updateReadOnly<dv::CfgType::LONG>("tsOffset", tsNowOffset);

				moduleNode.getRelativeNode("outputs/triggers/info/")
					.updateReadOnly<dv::CfgType::LONG>("tsOffset", tsNowOffset);

				moduleNode.getRelativeNode("outputs/imu/info/")
					.updateReadOnly<dv::CfgType::LONG>("tsOffset", tsNowOffset);
			}

			dvConvertToAedat4(special->getHeaderPointer(), moduleData);
		}

		if (data->size() == 1) {
			return;
		}

		if (data->getEventPacket(POLARITY_EVENT)) {
			dvConvertToAedat4(data->getEventPacket(POLARITY_EVENT)->getHeaderPointer(), moduleData);
		}

		// IMU6_EVENT is 2 here.
		if (data->getEventPacket(2)) {
			dvConvertToAedat4(data->getEventPacket(2)->getHeaderPointer(), moduleData);
		}
	}

private:
	static void moduleShutdownNotify(void *p) {
		dv::Cfg::Node moduleNode = static_cast<dvConfigNode>(p);

		// Ensure parent also shuts down (on disconnected device for example).
		moduleNode.putBool("running", false);
	}

	void sendDefaultConfiguration(const struct caer_dvs132s_info *devInfo) {
		// Send cAER configuration to libcaer and device.
		biasConfigSend();

		// Wait 200 ms for biases to stabilize.
		struct timespec biasEnSleep = {.tv_sec = 0, .tv_nsec = 200000000};
		nanosleep(&biasEnSleep, nullptr);

		systemConfigSend();
		usbConfigSend();
		multiplexerConfigSend();

		// Wait 50 ms for data transfer to be ready.
		struct timespec noDataSleep = {.tv_sec = 0, .tv_nsec = 50000000};
		nanosleep(&noDataSleep, nullptr);

		dvsConfigSend();
		imuConfigSend();
		externalInputConfigSend(devInfo);
	}

	static void biasConfigCreate(dv::RuntimeConfig &config) {
		// Chip biases, based on testing defaults.
		config.add("bias/PrBp",
			dv::ConfigOption::intOption("Bias PrBp (in pAmp) - Photoreceptor bandwidth.", 100 * 1000, 0, 1000000));

		config.add("bias/PrSFBpCoarse",
			dv::ConfigOption::intOption("Bias PrSFBp (in pAmp) - Photoreceptor bandwidth.", 1, 0, 1023));
		config.add("bias/PrSFBpFine",
			dv::ConfigOption::intOption("Bias PrSFBp (in pAmp) - Photoreceptor bandwidth.", 1, 0, 1023));

		config.add("bias/BlPuBp",
			dv::ConfigOption::intOption("Bias BlPuBp (in pAmp) - Bitline pull-up strength.", 0, 0, 1000000));
		config.add(
			"bias/BiasBufBp", dv::ConfigOption::intOption(
								  "Bias BiasBufBp (in pAmp) - P type bias buffer strength.", 10 * 1000, 0, 1000000));
		config.add("bias/OffBn",
			dv::ConfigOption::intOption("Bias OffBn (in pAmp) - Comparator OFF threshold.", 200, 0, 1000000));
		config.add("bias/DiffBn",
			dv::ConfigOption::intOption("Bias DiffBn (in pAmp) - Delta amplifier strength.", 10 * 1000, 0, 1000000));
		config.add("bias/OnBn",
			dv::ConfigOption::intOption("Bias OnBn (in pAmp) - Comparator ON threshold.", 400 * 1000, 0, 1000000));
		config.add("bias/CasBn",
			dv::ConfigOption::intOption(
				"Bias CasBn (in pAmp) - Cascode for delta amplifier and comparator.", 400 * 1000, 0, 1000000));
		config.add("bias/DPBn", dv::ConfigOption::intOption("Bias DPBn (in pAmp) - In-pixel direct path current limit.",
									100 * 1000, 0, 1000000));
		config.add(
			"bias/BiasBufBn", dv::ConfigOption::intOption(
								  "Bias BiasBufBn (in pAmp) - N type bias buffer strength.", 10 * 1000, 0, 1000000));
		config.add("bias/ABufBn",
			dv::ConfigOption::intOption("Bias ABufBn (in pAmp) - Diagnostic analog buffer strength.", 0, 0, 1000000));

		config.add("bias/BiasEnable", dv::ConfigOption::boolOption("Enable bias generator to power chip.", true));

		config.setPriorityOptions({"bias/PrSFBpCoarse", "bias/PrSFBpFine", "bias/PrBp"});
	}

	void biasConfigSend() {
		device.configSet(DVS132S_CONFIG_BIAS, DVS132S_CONFIG_BIAS_PRBP,
			caerBiasCoarseFine1024Generate(
				caerBiasCoarseFine1024FromCurrent(static_cast<uint32_t>(config.getInt("bias/PrBp")))));

		struct caer_bias_coarsefine1024 prSF;
		prSF.coarseValue = static_cast<uint16_t>(config.getInt("bias/PrSFBpCoarse"));
		prSF.fineValue   = static_cast<uint16_t>(config.getInt("bias/PrSFBpFine"));
		device.configSet(DVS132S_CONFIG_BIAS, DVS132S_CONFIG_BIAS_PRSFBP, caerBiasCoarseFine1024Generate(prSF));

		device.configSet(DVS132S_CONFIG_BIAS, DVS132S_CONFIG_BIAS_BLPUBP,
			caerBiasCoarseFine1024Generate(
				caerBiasCoarseFine1024FromCurrent(static_cast<uint32_t>(config.getInt("bias/BlPuBp")))));
		device.configSet(DVS132S_CONFIG_BIAS, DVS132S_CONFIG_BIAS_BIASBUFBP,
			caerBiasCoarseFine1024Generate(
				caerBiasCoarseFine1024FromCurrent(static_cast<uint32_t>(config.getInt("bias/BiasBufBp")))));
		device.configSet(DVS132S_CONFIG_BIAS, DVS132S_CONFIG_BIAS_CASBN,
			caerBiasCoarseFine1024Generate(
				caerBiasCoarseFine1024FromCurrent(static_cast<uint32_t>(config.getInt("bias/CasBn")))));
		device.configSet(DVS132S_CONFIG_BIAS, DVS132S_CONFIG_BIAS_DPBN,
			caerBiasCoarseFine1024Generate(
				caerBiasCoarseFine1024FromCurrent(static_cast<uint32_t>(config.getInt("bias/DPBn")))));
		device.configSet(DVS132S_CONFIG_BIAS, DVS132S_CONFIG_BIAS_BIASBUFBN,
			caerBiasCoarseFine1024Generate(
				caerBiasCoarseFine1024FromCurrent(static_cast<uint32_t>(config.getInt("bias/BiasBufBn")))));
		device.configSet(DVS132S_CONFIG_BIAS, DVS132S_CONFIG_BIAS_ABUFBN,
			caerBiasCoarseFine1024Generate(
				caerBiasCoarseFine1024FromCurrent(static_cast<uint32_t>(config.getInt("bias/ABufBn")))));
		device.configSet(DVS132S_CONFIG_BIAS, DVS132S_CONFIG_BIAS_OFFBN,
			caerBiasCoarseFine1024Generate(
				caerBiasCoarseFine1024FromCurrent(static_cast<uint32_t>(config.getInt("bias/OffBn")))));
		device.configSet(DVS132S_CONFIG_BIAS, DVS132S_CONFIG_BIAS_DIFFBN,
			caerBiasCoarseFine1024Generate(
				caerBiasCoarseFine1024FromCurrent(static_cast<uint32_t>(config.getInt("bias/DiffBn")))));
		device.configSet(DVS132S_CONFIG_BIAS, DVS132S_CONFIG_BIAS_ONBN,
			caerBiasCoarseFine1024Generate(
				caerBiasCoarseFine1024FromCurrent(static_cast<uint32_t>(config.getInt("bias/OnBn")))));

		device.configSet(DVS132S_CONFIG_MUX, DVS132S_CONFIG_MUX_RUN_CHIP, config.getBool("bias/BiasEnable"));
	}

	static void biasConfigListener(dvConfigNode node, void *userData, enum dvConfigAttributeEvents event,
		const char *changeKey, enum dvConfigAttributeType changeType, union dvConfigAttributeValue changeValue) {
		auto device = static_cast<libcaer::devices::dvs132s *>(userData);

		dv::Cfg::Node thisNode{node};
		std::string key{changeKey};

		if (event == DVCFG_ATTRIBUTE_MODIFIED) {
			if (changeType == DVCFG_TYPE_INT && key == "PrBp") {
				device->configSet(DVS132S_CONFIG_BIAS, DVS132S_CONFIG_BIAS_PRBP,
					caerBiasCoarseFine1024Generate(
						caerBiasCoarseFine1024FromCurrent(static_cast<uint32_t>(changeValue.iint))));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "PrSFBpCoarse") {
				struct caer_bias_coarsefine1024 prSF;
				prSF.coarseValue = static_cast<uint16_t>(changeValue.iint);
				prSF.fineValue   = static_cast<uint16_t>(thisNode.getInt("PrSFBpFine"));
				device->configSet(
					DVS132S_CONFIG_BIAS, DVS132S_CONFIG_BIAS_PRSFBP, caerBiasCoarseFine1024Generate(prSF));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "PrSFBpFine") {
				struct caer_bias_coarsefine1024 prSF;
				prSF.coarseValue = static_cast<uint16_t>(thisNode.getInt("PrSFBpCoarse"));
				prSF.fineValue   = static_cast<uint16_t>(changeValue.iint);
				device->configSet(
					DVS132S_CONFIG_BIAS, DVS132S_CONFIG_BIAS_PRSFBP, caerBiasCoarseFine1024Generate(prSF));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "BlPuBp") {
				device->configSet(DVS132S_CONFIG_BIAS, DVS132S_CONFIG_BIAS_BLPUBP,
					caerBiasCoarseFine1024Generate(
						caerBiasCoarseFine1024FromCurrent(static_cast<uint32_t>(changeValue.iint))));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "BiasBufBp") {
				device->configSet(DVS132S_CONFIG_BIAS, DVS132S_CONFIG_BIAS_BIASBUFBP,
					caerBiasCoarseFine1024Generate(
						caerBiasCoarseFine1024FromCurrent(static_cast<uint32_t>(changeValue.iint))));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "OffBn") {
				device->configSet(DVS132S_CONFIG_BIAS, DVS132S_CONFIG_BIAS_OFFBN,
					caerBiasCoarseFine1024Generate(
						caerBiasCoarseFine1024FromCurrent(static_cast<uint32_t>(changeValue.iint))));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "DiffBn") {
				device->configSet(DVS132S_CONFIG_BIAS, DVS132S_CONFIG_BIAS_DIFFBN,
					caerBiasCoarseFine1024Generate(
						caerBiasCoarseFine1024FromCurrent(static_cast<uint32_t>(changeValue.iint))));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "OnBn") {
				device->configSet(DVS132S_CONFIG_BIAS, DVS132S_CONFIG_BIAS_ONBN,
					caerBiasCoarseFine1024Generate(
						caerBiasCoarseFine1024FromCurrent(static_cast<uint32_t>(changeValue.iint))));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "CasBn") {
				device->configSet(DVS132S_CONFIG_BIAS, DVS132S_CONFIG_BIAS_CASBN,
					caerBiasCoarseFine1024Generate(
						caerBiasCoarseFine1024FromCurrent(static_cast<uint32_t>(changeValue.iint))));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "DPBn") {
				device->configSet(DVS132S_CONFIG_BIAS, DVS132S_CONFIG_BIAS_DPBN,
					caerBiasCoarseFine1024Generate(
						caerBiasCoarseFine1024FromCurrent(static_cast<uint32_t>(changeValue.iint))));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "BiasBufBn") {
				device->configSet(DVS132S_CONFIG_BIAS, DVS132S_CONFIG_BIAS_BIASBUFBN,
					caerBiasCoarseFine1024Generate(
						caerBiasCoarseFine1024FromCurrent(static_cast<uint32_t>(changeValue.iint))));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "ABufBn") {
				device->configSet(DVS132S_CONFIG_BIAS, DVS132S_CONFIG_BIAS_ABUFBN,
					caerBiasCoarseFine1024Generate(
						caerBiasCoarseFine1024FromCurrent(static_cast<uint32_t>(changeValue.iint))));
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "BiasEnable") {
				device->configSet(DVS132S_CONFIG_MUX, DVS132S_CONFIG_MUX_RUN_CHIP, changeValue.boolean);
			}
		}
	}

	static void multiplexerConfigCreate(dv::RuntimeConfig &config) {
		// Subsystem 0: Multiplexer
		config.add("multiplexer/Run", dv::ConfigOption::boolOption("Enable multiplexer state machine.", true));
		config.add("multiplexer/TimestampRun", dv::ConfigOption::boolOption("Enable µs-timestamp generation.", true));
		config.add("multiplexer/TimestampReset",
			dv::ConfigOption::buttonOption("Reset timestamps to zero.", "Reset timestamps"));
		config.add("multiplexer/DropDVSOnTransferStall",
			dv::ConfigOption::boolOption("Drop Polarity events when USB FIFO is full.", false));
		config.add("multiplexer/DropExtInputOnTransferStall",
			dv::ConfigOption::boolOption("Drop ExternalInput events when USB FIFO is full.", true));

		config.setPriorityOptions({"multiplexer/"});
	}

	void multiplexerConfigCreateDynamic(const struct caer_dvs132s_info *devInfo) {
		// Device event statistics.
		if (devInfo->muxHasStatistics) {
			config.add("statistics/muxDroppedDVS",
				dv::ConfigOption::statisticOption("Number of dropped DVS events due to USB full."));
			config.add("statistics/muxDroppedExtInput",
				dv::ConfigOption::statisticOption("Number of dropped External Input events due to USB full."));

			auto statNode = moduleNode.getRelativeNode("statistics/");

			statNode.attributeUpdaterAdd("muxDroppedDVS", dv::CfgType::LONG, &statisticsUpdater, &device);
			statNode.attributeUpdaterAdd("muxDroppedExtInput", dv::CfgType::LONG, &statisticsUpdater, &device);

			config.setPriorityOptions({"statistics/"});
		}
	}

	void multiplexerConfigSend() {
		device.configSet(DVS132S_CONFIG_MUX, DVS132S_CONFIG_MUX_TIMESTAMP_RESET, false);
		config.setBool("multiplexer/TimestampReset", false); // Ensure default value is reset.
		device.configSet(DVS132S_CONFIG_MUX, DVS132S_CONFIG_MUX_DROP_DVS_ON_TRANSFER_STALL,
			config.getBool("multiplexer/DropDVSOnTransferStall"));
		device.configSet(DVS132S_CONFIG_MUX, DVS132S_CONFIG_MUX_DROP_EXTINPUT_ON_TRANSFER_STALL,
			config.getBool("multiplexer/DropExtInputOnTransferStall"));
		device.configSet(
			DVS132S_CONFIG_MUX, DVS132S_CONFIG_MUX_TIMESTAMP_RUN, config.getBool("multiplexer/TimestampRun"));
		device.configSet(DVS132S_CONFIG_MUX, DVS132S_CONFIG_MUX_RUN, config.getBool("multiplexer/Run"));
	}

	static void multiplexerConfigListener(dvConfigNode node, void *userData, enum dvConfigAttributeEvents event,
		const char *changeKey, enum dvConfigAttributeType changeType, union dvConfigAttributeValue changeValue) {
		UNUSED_ARGUMENT(node);

		auto device = static_cast<libcaer::devices::dvs132s *>(userData);

		std::string key{changeKey};

		if (event == DVCFG_ATTRIBUTE_MODIFIED) {
			if (changeType == DVCFG_TYPE_BOOL && key == "TimestampReset" && changeValue.boolean) {
				device->configSet(DVS132S_CONFIG_MUX, DVS132S_CONFIG_MUX_TIMESTAMP_RESET, changeValue.boolean);

				dvConfigNodeAttributeBooleanReset(node, changeKey);
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "DropDVSOnTransferStall") {
				device->configSet(
					DVS132S_CONFIG_MUX, DVS132S_CONFIG_MUX_DROP_DVS_ON_TRANSFER_STALL, changeValue.boolean);
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "DropExtInputOnTransferStall") {
				device->configSet(
					DVS132S_CONFIG_MUX, DVS132S_CONFIG_MUX_DROP_EXTINPUT_ON_TRANSFER_STALL, changeValue.boolean);
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "TimestampRun") {
				device->configSet(DVS132S_CONFIG_MUX, DVS132S_CONFIG_MUX_TIMESTAMP_RUN, changeValue.boolean);
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "Run") {
				device->configSet(DVS132S_CONFIG_MUX, DVS132S_CONFIG_MUX_RUN, changeValue.boolean);
			}
		}
	}

	static void dvsConfigCreate(dv::RuntimeConfig &config) {
		// Subsystem 1: DVS
		config.add("dvs/Run", dv::ConfigOption::boolOption("Enable DVS (Polarity events).", true));
		config.add("dvs/WaitOnTransferStall", dv::ConfigOption::boolOption("On event FIFO full, pause readout.", true));
		config.add("dvs/FilterAtLeast2Unsigned",
			dv::ConfigOption::boolOption(
				"Only read events from a group of four pixels if at least two are active, regardless of polarity."));
		config.add("dvs/FilterNotAll4Unsigned",
			dv::ConfigOption::boolOption(
				"Only read events from a group of four pixels if not all four are active, regardless of polarity."));
		config.add("dvs/FilterAtLeast2Signed",
			dv::ConfigOption::boolOption(
				"Only read events from a group of four pixels if at least two are active and have the same polarity."));
		config.add("dvs/FilterNotAll4Signed",
			dv::ConfigOption::boolOption(
				"Only read events from a group of four pixels if not all four are active and have the same polarity."));
		config.add(
			"dvs/RestartTime", dv::ConfigOption::intOption("Restart pulse length, in µs.", 100, 1, ((0x01 << 7) - 1)));
		config.add("dvs/CaptureInterval",
			dv::ConfigOption::intOption("Time interval between DVS readouts, in µs.", 500, 1, ((0x01 << 21) - 1)));
		config.add("dvs/RowEnable", dv::ConfigOption::stringOption("Enable rows to be read-out (ROI filter).",
										"111111111111111111111111111111111111111111111111111111111111111111", 66, 66));
		config.add("dvs/ColumnEnable", dv::ConfigOption::stringOption("Enable columns to be read-out (ROI filter).",
										   "1111111111111111111111111111111111111111111111111111", 52, 52));

		config.setPriorityOptions({"dvs/"});
	}

	void dvsConfigCreateDynamic(const struct caer_dvs132s_info *devInfo) {
		if (devInfo->dvsHasStatistics) {
			config.add("statistics/dvsTransactionsSuccess",
				dv::ConfigOption::statisticOption("Number of groups of events received successfully."));
			config.add("statistics/dvsTransactionsSkipped",
				dv::ConfigOption::statisticOption("Number of dropped groups of events due to full buffers."));
			config.add("statistics/dvsTransactionsAll",
				dv::ConfigOption::statisticOption("Total number of event groups seen."));
			config.add("statistics/dvsTransactionsErrored",
				dv::ConfigOption::statisticOption("Number of erroneous groups of events."));

			auto statNode = moduleNode.getRelativeNode("statistics/");

			statNode.attributeUpdaterAdd("dvsTransactionsSuccess", dv::CfgType::LONG, &statisticsUpdater, &device);
			statNode.attributeUpdaterAdd("dvsTransactionsSkipped", dv::CfgType::LONG, &statisticsUpdater, &device);
			statNode.attributeUpdaterAdd("dvsTransactionsAll", dv::CfgType::LONG, &statisticsUpdater, &device);
			statNode.attributeUpdaterAdd("dvsTransactionsErrored", dv::CfgType::LONG, &statisticsUpdater, &device);

			config.setPriorityOptions({"statistics/"});
		}
	}

	void dvsConfigSend() {
		device.configSet(
			DVS132S_CONFIG_DVS, DVS132S_CONFIG_DVS_WAIT_ON_TRANSFER_STALL, config.getBool("dvs/WaitOnTransferStall"));
		device.configSet(DVS132S_CONFIG_DVS, DVS132S_CONFIG_DVS_FILTER_AT_LEAST_2_UNSIGNED,
			config.getBool("dvs/FilterAtLeast2Unsigned"));
		device.configSet(DVS132S_CONFIG_DVS, DVS132S_CONFIG_DVS_FILTER_NOT_ALL_4_UNSIGNED,
			config.getBool("dvs/FilterNotAll4Unsigned"));
		device.configSet(DVS132S_CONFIG_DVS, DVS132S_CONFIG_DVS_FILTER_AT_LEAST_2_SIGNED,
			config.getBool("dvs/FilterAtLeast2Signed"));
		device.configSet(
			DVS132S_CONFIG_DVS, DVS132S_CONFIG_DVS_FILTER_NOT_ALL_4_SIGNED, config.getBool("dvs/FilterNotAll4Signed"));

		device.configSet(DVS132S_CONFIG_DVS, DVS132S_CONFIG_DVS_RESTART_TIME,
			static_cast<uint32_t>(config.getInt("dvs/RestartTime")));
		device.configSet(DVS132S_CONFIG_DVS, DVS132S_CONFIG_DVS_CAPTURE_INTERVAL,
			static_cast<uint32_t>(config.getInt("dvs/CaptureInterval")));

		// Parse string bitfields into corresponding integer bitfields for device.
		auto rowEnableStr = config.getString("dvs/RowEnable");

		dvsRowEnableParse(rowEnableStr, &device);

		// Parse string bitfields into corresponding integer bitfields for device.
		auto columnEnableStr = config.getString("dvs/ColumnEnable");

		dvsColumnEnableParse(columnEnableStr, &device);

		// Wait 5 ms for row/column enables to have been sent out.
		struct timespec enableSleep = {.tv_sec = 0, .tv_nsec = 5000000};
		nanosleep(&enableSleep, nullptr);

		device.configSet(DVS132S_CONFIG_DVS, DVS132S_CONFIG_DVS_RUN, config.getBool("dvs/Run"));
	}

	static void dvsConfigListener(dvConfigNode node, void *userData, enum dvConfigAttributeEvents event,
		const char *changeKey, enum dvConfigAttributeType changeType, union dvConfigAttributeValue changeValue) {
		UNUSED_ARGUMENT(node);

		auto device = static_cast<libcaer::devices::dvs132s *>(userData);

		std::string key{changeKey};

		if (event == DVCFG_ATTRIBUTE_MODIFIED) {
			if (changeType == DVCFG_TYPE_BOOL && key == "WaitOnTransferStall") {
				device->configSet(DVS132S_CONFIG_DVS, DVS132S_CONFIG_DVS_WAIT_ON_TRANSFER_STALL, changeValue.boolean);
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "FilterAtLeast2Unsigned") {
				device->configSet(
					DVS132S_CONFIG_DVS, DVS132S_CONFIG_DVS_FILTER_AT_LEAST_2_UNSIGNED, changeValue.boolean);
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "FilterNotAll4Unsigned") {
				device->configSet(
					DVS132S_CONFIG_DVS, DVS132S_CONFIG_DVS_FILTER_NOT_ALL_4_UNSIGNED, changeValue.boolean);
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "FilterAtLeast2Signed") {
				device->configSet(DVS132S_CONFIG_DVS, DVS132S_CONFIG_DVS_FILTER_AT_LEAST_2_SIGNED, changeValue.boolean);
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "FilterNotAll4Signed") {
				device->configSet(DVS132S_CONFIG_DVS, DVS132S_CONFIG_DVS_FILTER_NOT_ALL_4_SIGNED, changeValue.boolean);
			}
			else if (changeType == DVCFG_TYPE_INT && key == "RestartTime") {
				device->configSet(
					DVS132S_CONFIG_DVS, DVS132S_CONFIG_DVS_RESTART_TIME, static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "CaptureInterval") {
				device->configSet(
					DVS132S_CONFIG_DVS, DVS132S_CONFIG_DVS_CAPTURE_INTERVAL, static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_STRING && key == "RowEnable") {
				// Parse string bitfields into corresponding integer bitfields for device.
				dvsRowEnableParse(changeValue.string, device);
			}
			else if (changeType == DVCFG_TYPE_STRING && key == "ColumnEnable") {
				// Parse string bitfields into corresponding integer bitfields for device.
				dvsColumnEnableParse(changeValue.string, device);
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "Run") {
				device->configSet(DVS132S_CONFIG_DVS, DVS132S_CONFIG_DVS_RUN, changeValue.boolean);
			}
		}
	}

	static void imuConfigCreate(dv::RuntimeConfig &config) {
		// Subsystem 3: IMU
		config.add("imu/RunAccelerometer", dv::ConfigOption::boolOption("Enable accelerometer.", true));
		config.add("imu/RunGyroscope", dv::ConfigOption::boolOption("Enable gyroscope.", true));
		config.add("imu/RunTemperature", dv::ConfigOption::boolOption("Enable temperature sensor.", true));
		config.add(
			"imu/AccelDataRate", dv::ConfigOption::listOption("Accelerometer bandwidth configuration.", "800 Hz",
									 {"12.5 Hz", "25 Hz", "50 Hz", "100 Hz", "200 Hz", "400 Hz", "800 Hz", "1600 Hz"}));
		config.add("imu/AccelFilter",
			dv::ConfigOption::listOption("Accelerometer filter configuration.", "Normal", {"Normal", "OSR2", "OSR4"}));
		config.add("imu/AccelRange",
			dv::ConfigOption::listOption("Accelerometer range configuration.", "±4G", {"±2G", "±4G", "±8G", "±16G"}));
		config.add(
			"imu/GyroDataRate", dv::ConfigOption::listOption("Gyroscope bandwidth configuration.", "800 Hz",
									{"25 Hz", "50 Hz", "100 Hz", "200 Hz", "400 Hz", "800 Hz", "1600 Hz", "3200 Hz"}));
		config.add("imu/GyroFilter",
			dv::ConfigOption::listOption("Gyroscope filter configuration.", "Normal", {"Normal", "OSR2", "OSR4"}));
		config.add("imu/GyroRange", dv::ConfigOption::listOption("Gyroscope range configuration.", "±500°/s",
										{"±125°/s", "±250°/s", "±500°/s", "±1000°/s", "±2000°/s"}));

		config.setPriorityOptions({"imu/RunAccelerometer", "imu/RunGyroscope"});
	}

	void imuConfigSend() {
		device.configSet(DVS132S_CONFIG_IMU, DVS132S_CONFIG_IMU_ACCEL_DATA_RATE,
			mapAccelDataRate(config.getString("imu/AccelDataRate")));
		device.configSet(
			DVS132S_CONFIG_IMU, DVS132S_CONFIG_IMU_ACCEL_FILTER, mapAccelFilter(config.getString("imu/AccelFilter")));
		device.configSet(
			DVS132S_CONFIG_IMU, DVS132S_CONFIG_IMU_ACCEL_RANGE, mapAccelRange(config.getString("imu/AccelRange")));
		device.configSet(DVS132S_CONFIG_IMU, DVS132S_CONFIG_IMU_GYRO_DATA_RATE,
			mapGyroDataRate(config.getString("imu/GyroDataRate")));
		device.configSet(
			DVS132S_CONFIG_IMU, DVS132S_CONFIG_IMU_GYRO_FILTER, mapGyroFilter(config.getString("imu/GyroFilter")));
		device.configSet(
			DVS132S_CONFIG_IMU, DVS132S_CONFIG_IMU_GYRO_RANGE, mapGyroRange(config.getString("imu/GyroRange")));

		device.configSet(
			DVS132S_CONFIG_IMU, DVS132S_CONFIG_IMU_RUN_ACCELEROMETER, config.getBool("imu/RunAccelerometer"));
		device.configSet(DVS132S_CONFIG_IMU, DVS132S_CONFIG_IMU_RUN_GYROSCOPE, config.getBool("imu/RunGyroscope"));
		device.configSet(DVS132S_CONFIG_IMU, DVS132S_CONFIG_IMU_RUN_TEMPERATURE, config.getBool("imu/RunTemperature"));
	}

	static void imuConfigListener(dvConfigNode node, void *userData, enum dvConfigAttributeEvents event,
		const char *changeKey, enum dvConfigAttributeType changeType, union dvConfigAttributeValue changeValue) {
		UNUSED_ARGUMENT(node);

		auto device = static_cast<libcaer::devices::dvs132s *>(userData);

		std::string key{changeKey};

		if (event == DVCFG_ATTRIBUTE_MODIFIED) {
			if (changeType == DVCFG_TYPE_STRING && key == "AccelDataRate") {
				device->configSet(
					DVS132S_CONFIG_IMU, DVS132S_CONFIG_IMU_ACCEL_DATA_RATE, mapAccelDataRate(changeValue.string));
			}
			else if (changeType == DVCFG_TYPE_STRING && key == "AccelFilter") {
				device->configSet(
					DVS132S_CONFIG_IMU, DVS132S_CONFIG_IMU_ACCEL_FILTER, mapAccelFilter(changeValue.string));
			}
			else if (changeType == DVCFG_TYPE_STRING && key == "AccelRange") {
				device->configSet(
					DVS132S_CONFIG_IMU, DVS132S_CONFIG_IMU_ACCEL_RANGE, mapAccelRange(changeValue.string));
			}
			else if (changeType == DVCFG_TYPE_STRING && key == "GyroDataRate") {
				device->configSet(
					DVS132S_CONFIG_IMU, DVS132S_CONFIG_IMU_GYRO_DATA_RATE, mapGyroDataRate(changeValue.string));
			}
			else if (changeType == DVCFG_TYPE_STRING && key == "GyroFilter") {
				device->configSet(
					DVS132S_CONFIG_IMU, DVS132S_CONFIG_IMU_GYRO_FILTER, mapGyroFilter(changeValue.string));
			}
			else if (changeType == DVCFG_TYPE_STRING && key == "GyroRange") {
				device->configSet(DVS132S_CONFIG_IMU, DVS132S_CONFIG_IMU_GYRO_RANGE, mapGyroRange(changeValue.string));
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "RunAccelerometer") {
				device->configSet(DVS132S_CONFIG_IMU, DVS132S_CONFIG_IMU_RUN_ACCELEROMETER, changeValue.boolean);
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "RunGyroscope") {
				device->configSet(DVS132S_CONFIG_IMU, DVS132S_CONFIG_IMU_RUN_GYROSCOPE, changeValue.boolean);
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "RunTemperature") {
				device->configSet(DVS132S_CONFIG_IMU, DVS132S_CONFIG_IMU_RUN_TEMPERATURE, changeValue.boolean);
			}
		}
	}

	static void externalInputConfigCreate(dv::RuntimeConfig &config) {
		// Subsystem 4: External Input
		config.add("externalInput/RunDetector", dv::ConfigOption::boolOption("Enable signal detector."));
		config.add("externalInput/DetectRisingEdges",
			dv::ConfigOption::boolOption("Emit special event if a rising edge is detected."));
		config.add("externalInput/DetectFallingEdges",
			dv::ConfigOption::boolOption("Emit special event if a falling edge is detected."));
		config.add(
			"externalInput/DetectPulses", dv::ConfigOption::boolOption("Emit special event if a pulse is detected."));
		config.add("externalInput/DetectPulsePolarity",
			dv::ConfigOption::boolOption("Polarity of the pulse to be detected.", true));
		config.add("externalInput/DetectPulseLength",
			dv::ConfigOption::intOption(
				"Minimal length of the pulse to be detected (in µs).", 10, 1, ((0x01 << 20) - 1)));

		config.setPriorityOptions({"externalInput/"});
	}

	void externalInputConfigCreateDynamic(const struct caer_dvs132s_info *devInfo) {
		if (devInfo->extInputHasGenerator) {
			config.add(
				"externalInput/RunGenerator", dv::ConfigOption::boolOption("Enable signal generator (PWM-like)."));
			config.add("externalInput/GeneratePulsePolarity",
				dv::ConfigOption::boolOption("Polarity of the generated pulse.", true));
			config.add("externalInput/GeneratePulseInterval",
				dv::ConfigOption::intOption(
					"Time interval between consecutive pulses (in µs).", 10, 1, ((0x01 << 20) - 1)));
			config.add("externalInput/GeneratePulseLength",
				dv::ConfigOption::intOption("Time length of a pulse (in µs).", 5, 1, ((0x01 << 20) - 1)));
			config.add("externalInput/GenerateInjectOnRisingEdge",
				dv::ConfigOption::boolOption("Emit a special event when a rising edge is generated."));
			config.add("externalInput/GenerateInjectOnFallingEdge",
				dv::ConfigOption::boolOption("Emit a special event when a falling edge is generated."));
		}
	}

	void externalInputConfigSend(const struct caer_dvs132s_info *devInfo) {
		device.configSet(DVS132S_CONFIG_EXTINPUT, DVS132S_CONFIG_EXTINPUT_DETECT_RISING_EDGES,
			config.getBool("externalInput/DetectRisingEdges"));
		device.configSet(DVS132S_CONFIG_EXTINPUT, DVS132S_CONFIG_EXTINPUT_DETECT_FALLING_EDGES,
			config.getBool("externalInput/DetectFallingEdges"));
		device.configSet(DVS132S_CONFIG_EXTINPUT, DVS132S_CONFIG_EXTINPUT_DETECT_PULSES,
			config.getBool("externalInput/DetectPulses"));
		device.configSet(DVS132S_CONFIG_EXTINPUT, DVS132S_CONFIG_EXTINPUT_DETECT_PULSE_POLARITY,
			config.getBool("externalInput/DetectPulsePolarity"));
		device.configSet(DVS132S_CONFIG_EXTINPUT, DVS132S_CONFIG_EXTINPUT_DETECT_PULSE_LENGTH,
			static_cast<uint32_t>(config.getInt("externalInput/DetectPulseLength")));
		device.configSet(
			DVS132S_CONFIG_EXTINPUT, DVS132S_CONFIG_EXTINPUT_RUN_DETECTOR, config.getBool("externalInput/RunDetector"));

		if (devInfo->extInputHasGenerator) {
			device.configSet(DVS132S_CONFIG_EXTINPUT, DVS132S_CONFIG_EXTINPUT_GENERATE_PULSE_POLARITY,
				config.getBool("externalInput/GeneratePulsePolarity"));
			device.configSet(DVS132S_CONFIG_EXTINPUT, DVS132S_CONFIG_EXTINPUT_GENERATE_PULSE_INTERVAL,
				static_cast<uint32_t>(config.getInt("externalInput/GeneratePulseInterval")));
			device.configSet(DVS132S_CONFIG_EXTINPUT, DVS132S_CONFIG_EXTINPUT_GENERATE_PULSE_LENGTH,
				static_cast<uint32_t>(config.getInt("externalInput/GeneratePulseLength")));
			device.configSet(DVS132S_CONFIG_EXTINPUT, DVS132S_CONFIG_EXTINPUT_GENERATE_INJECT_ON_RISING_EDGE,
				config.getBool("externalInput/GenerateInjectOnRisingEdge"));
			device.configSet(DVS132S_CONFIG_EXTINPUT, DVS132S_CONFIG_EXTINPUT_GENERATE_INJECT_ON_FALLING_EDGE,
				config.getBool("externalInput/GenerateInjectOnFallingEdge"));
			device.configSet(DVS132S_CONFIG_EXTINPUT, DVS132S_CONFIG_EXTINPUT_RUN_GENERATOR,
				config.getBool("externalInput/RunGenerator"));
		}
	}

	static void externalInputConfigListener(dvConfigNode node, void *userData, enum dvConfigAttributeEvents event,
		const char *changeKey, enum dvConfigAttributeType changeType, union dvConfigAttributeValue changeValue) {
		UNUSED_ARGUMENT(node);

		auto device = static_cast<libcaer::devices::dvs132s *>(userData);

		std::string key{changeKey};

		if (event == DVCFG_ATTRIBUTE_MODIFIED) {
			if (changeType == DVCFG_TYPE_BOOL && key == "DetectRisingEdges") {
				device->configSet(
					DVS132S_CONFIG_EXTINPUT, DVS132S_CONFIG_EXTINPUT_DETECT_RISING_EDGES, changeValue.boolean);
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "DetectFallingEdges") {
				device->configSet(
					DVS132S_CONFIG_EXTINPUT, DVS132S_CONFIG_EXTINPUT_DETECT_FALLING_EDGES, changeValue.boolean);
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "DetectPulses") {
				device->configSet(DVS132S_CONFIG_EXTINPUT, DVS132S_CONFIG_EXTINPUT_DETECT_PULSES, changeValue.boolean);
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "DetectPulsePolarity") {
				device->configSet(
					DVS132S_CONFIG_EXTINPUT, DVS132S_CONFIG_EXTINPUT_DETECT_PULSE_POLARITY, changeValue.boolean);
			}
			else if (changeType == DVCFG_TYPE_INT && key == "DetectPulseLength") {
				device->configSet(DVS132S_CONFIG_EXTINPUT, DVS132S_CONFIG_EXTINPUT_DETECT_PULSE_LENGTH,
					static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "RunDetector") {
				device->configSet(DVS132S_CONFIG_EXTINPUT, DVS132S_CONFIG_EXTINPUT_RUN_DETECTOR, changeValue.boolean);
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "GeneratePulsePolarity") {
				device->configSet(
					DVS132S_CONFIG_EXTINPUT, DVS132S_CONFIG_EXTINPUT_GENERATE_PULSE_POLARITY, changeValue.boolean);
			}
			else if (changeType == DVCFG_TYPE_INT && key == "GeneratePulseInterval") {
				device->configSet(DVS132S_CONFIG_EXTINPUT, DVS132S_CONFIG_EXTINPUT_GENERATE_PULSE_INTERVAL,
					static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "GeneratePulseLength") {
				device->configSet(DVS132S_CONFIG_EXTINPUT, DVS132S_CONFIG_EXTINPUT_GENERATE_PULSE_LENGTH,
					static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "GenerateInjectOnRisingEdge") {
				device->configSet(DVS132S_CONFIG_EXTINPUT, DVS132S_CONFIG_EXTINPUT_GENERATE_INJECT_ON_RISING_EDGE,
					changeValue.boolean);
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "GenerateInjectOnFallingEdge") {
				device->configSet(DVS132S_CONFIG_EXTINPUT, DVS132S_CONFIG_EXTINPUT_GENERATE_INJECT_ON_FALLING_EDGE,
					changeValue.boolean);
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "RunGenerator") {
				device->configSet(DVS132S_CONFIG_EXTINPUT, DVS132S_CONFIG_EXTINPUT_RUN_GENERATOR, changeValue.boolean);
			}
		}
	}

	static void usbConfigCreate(dv::RuntimeConfig &config) {
		// Subsystem 9: FX2/3 USB Configuration and USB buffer settings.
		config.add(
			"usb/Run", dv::ConfigOption::boolOption("Enable the USB state machine (FPGA to USB data exchange).", true));
		config.add("usb/EarlyPacketDelay",
			dv::ConfigOption::intOption(
				"Send early USB packets if this timeout is reached (in 125µs time-slices).", 8, 1, 8000));

		// USB buffer settings.
		config.add("usb/BufferNumber", dv::ConfigOption::intOption("Number of USB transfers.", 8, 2, 128));
		config.add("usb/BufferSize",
			dv::ConfigOption::intOption("Size in bytes of data buffers for USB transfers.", 8192, 512, 32768));

		config.setPriorityOptions({"usb/"});
	}

	void usbConfigSend() {
		device.configSet(CAER_HOST_CONFIG_USB, CAER_HOST_CONFIG_USB_BUFFER_NUMBER,
			static_cast<uint32_t>(config.getInt("usb/BufferNumber")));
		device.configSet(CAER_HOST_CONFIG_USB, CAER_HOST_CONFIG_USB_BUFFER_SIZE,
			static_cast<uint32_t>(config.getInt("usb/BufferSize")));

		device.configSet(DVS132S_CONFIG_USB, DVS132S_CONFIG_USB_EARLY_PACKET_DELAY,
			static_cast<uint32_t>(config.getInt("usb/EarlyPacketDelay")));
		device.configSet(DVS132S_CONFIG_USB, DVS132S_CONFIG_USB_RUN, config.getBool("usb/Run"));
	}

	static void usbConfigListener(dvConfigNode node, void *userData, enum dvConfigAttributeEvents event,
		const char *changeKey, enum dvConfigAttributeType changeType, union dvConfigAttributeValue changeValue) {
		UNUSED_ARGUMENT(node);

		auto device = static_cast<libcaer::devices::dvs132s *>(userData);

		std::string key{changeKey};

		if (event == DVCFG_ATTRIBUTE_MODIFIED) {
			if (changeType == DVCFG_TYPE_INT && key == "BufferNumber") {
				device->configSet(
					CAER_HOST_CONFIG_USB, CAER_HOST_CONFIG_USB_BUFFER_NUMBER, static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "BufferSize") {
				device->configSet(
					CAER_HOST_CONFIG_USB, CAER_HOST_CONFIG_USB_BUFFER_SIZE, static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "EarlyPacketDelay") {
				device->configSet(
					DVS132S_CONFIG_USB, DVS132S_CONFIG_USB_EARLY_PACKET_DELAY, static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "Run") {
				device->configSet(DVS132S_CONFIG_USB, DVS132S_CONFIG_USB_RUN, changeValue.boolean);
			}
		}
	}

	static void systemConfigCreate(dv::RuntimeConfig &config) {
		// Packet settings (size (in events) and time interval (in µs)).
		config.add("system/PacketContainerMaxPacketSize",
			dv::ConfigOption::intOption("Maximum packet size in events, when any packet reaches this size, the "
										"EventPacketContainer is sent for processing.",
				0, 0, 10 * 1024 * 1024));
		config.add("system/PacketContainerInterval",
			dv::ConfigOption::intOption("Time interval in µs, each sent EventPacketContainer will span this interval.",
				10000, 1, 120 * 1000 * 1000));

		// Ring-buffer setting (only changes value on module init/shutdown cycles).
		config.add("system/DataExchangeBufferSize",
			dv::ConfigOption::intOption(
				"Size of EventPacketContainer queue, used for transfers between data acquisition thread and mainloop.",
				64, 8, 1024));

		config.setPriorityOptions({"system/"});
	}

	void systemConfigSend() {
		device.configSet(CAER_HOST_CONFIG_PACKETS, CAER_HOST_CONFIG_PACKETS_MAX_CONTAINER_PACKET_SIZE,
			static_cast<uint32_t>(config.getInt("system/PacketContainerMaxPacketSize")));
		device.configSet(CAER_HOST_CONFIG_PACKETS, CAER_HOST_CONFIG_PACKETS_MAX_CONTAINER_INTERVAL,
			static_cast<uint32_t>(config.getInt("system/PacketContainerInterval")));

		// Changes only take effect on module start!
		device.configSet(CAER_HOST_CONFIG_DATAEXCHANGE, CAER_HOST_CONFIG_DATAEXCHANGE_BUFFER_SIZE,
			static_cast<uint32_t>(config.getInt("system/DataExchangeBufferSize")));
	}

	static void systemConfigListener(dvConfigNode node, void *userData, enum dvConfigAttributeEvents event,
		const char *changeKey, enum dvConfigAttributeType changeType, union dvConfigAttributeValue changeValue) {
		UNUSED_ARGUMENT(node);

		auto device = static_cast<libcaer::devices::dvs132s *>(userData);

		std::string key{changeKey};
		if (event == DVCFG_ATTRIBUTE_MODIFIED) {
			if (changeType == DVCFG_TYPE_INT && key == "PacketContainerMaxPacketSize") {
				device->configSet(CAER_HOST_CONFIG_PACKETS, CAER_HOST_CONFIG_PACKETS_MAX_CONTAINER_PACKET_SIZE,
					static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "PacketContainerInterval") {
				device->configSet(CAER_HOST_CONFIG_PACKETS, CAER_HOST_CONFIG_PACKETS_MAX_CONTAINER_INTERVAL,
					static_cast<uint32_t>(changeValue.iint));
			}
		}
	}

	static void logLevelListener(dvConfigNode node, void *userData, enum dvConfigAttributeEvents event,
		const char *changeKey, enum dvConfigAttributeType changeType, union dvConfigAttributeValue changeValue) {
		UNUSED_ARGUMENT(node);

		auto device = static_cast<libcaer::devices::dvs132s *>(userData);

		std::string key{changeKey};

		if (event == DVCFG_ATTRIBUTE_MODIFIED && changeType == DVCFG_TYPE_STRING && key == "logLevel") {
			device->configSet(CAER_HOST_CONFIG_LOG, CAER_HOST_CONFIG_LOG_LEVEL,
				static_cast<uint32_t>(dv::LoggerInternal::logLevelNameToInteger(changeValue.string)));
		}
	}

	static union dvConfigAttributeValue statisticsUpdater(
		void *userData, const char *key, enum dvConfigAttributeType type) {
		UNUSED_ARGUMENT(type); // We know all statistics are always LONG.

		auto device = static_cast<libcaer::devices::dvs132s *>(userData);

		std::string keyStr{key};

		union dvConfigAttributeValue statisticValue = {.ilong = 0};

		try {
			if (keyStr == "muxDroppedDVS") {
				device->configGet64(DVS132S_CONFIG_MUX, DVS132S_CONFIG_MUX_STATISTICS_DVS_DROPPED,
					reinterpret_cast<uint64_t *>(&statisticValue.ilong));
			}
			else if (keyStr == "muxDroppedExtInput") {
				device->configGet64(DVS132S_CONFIG_MUX, DVS132S_CONFIG_MUX_STATISTICS_EXTINPUT_DROPPED,
					reinterpret_cast<uint64_t *>(&statisticValue.ilong));
			}
			else if (keyStr == "dvsTransactionsSuccess") {
				device->configGet64(DVS132S_CONFIG_DVS, DVS132S_CONFIG_DVS_STATISTICS_TRANSACTIONS_SUCCESS,
					reinterpret_cast<uint64_t *>(&statisticValue.ilong));
			}
			else if (keyStr == "dvsTransactionsSkipped") {
				device->configGet64(DVS132S_CONFIG_DVS, DVS132S_CONFIG_DVS_STATISTICS_TRANSACTIONS_SKIPPED,
					reinterpret_cast<uint64_t *>(&statisticValue.ilong));
			}
			else if (keyStr == "dvsTransactionsAll") {
				uint64_t success = 0;
				device->configGet64(DVS132S_CONFIG_DVS, DVS132S_CONFIG_DVS_STATISTICS_TRANSACTIONS_SUCCESS, &success);

				uint64_t skipped = 0;
				device->configGet64(DVS132S_CONFIG_DVS, DVS132S_CONFIG_DVS_STATISTICS_TRANSACTIONS_SKIPPED, &skipped);

				statisticValue.ilong = static_cast<int64_t>(success + skipped);
			}
			else if (keyStr == "dvsTransactionsErrored") {
				uint32_t statisticValue32 = 0;
				device->configGet(
					DVS132S_CONFIG_DVS, DVS132S_CONFIG_DVS_STATISTICS_TRANSACTIONS_ERRORED, &statisticValue32);
				statisticValue.ilong = statisticValue32;
			}
		}
		catch (const std::runtime_error &) {
			// Catch communication failures and ignore them.
		}

		return (statisticValue);
	}

	static inline void dvsRowEnableParse(const std::string &rowEnableStr, libcaer::devices::dvs132s *device) {
		size_t rowEnableIndex = 0;

		uint32_t rowInt31To0 = 0;

		for (size_t i = 0; i < 32; i++) {
			if (rowEnableStr[rowEnableIndex++] == '1') {
				rowInt31To0 |= static_cast<uint32_t>(0x01 << i);
			}
		}

		uint32_t rowInt63To32 = 0;

		for (size_t i = 0; i < 32; i++) {
			if (rowEnableStr[rowEnableIndex++] == '1') {
				rowInt63To32 |= static_cast<uint32_t>(0x01 << i);
			}
		}

		uint32_t rowInt65To64 = 0;

		for (size_t i = 0; i < 2; i++) {
			if (rowEnableStr[rowEnableIndex++] == '1') {
				rowInt65To64 |= static_cast<uint32_t>(0x01 << i);
			}
		}

		device->configSet(DVS132S_CONFIG_DVS, DVS132S_CONFIG_DVS_ROW_ENABLE_31_TO_0, rowInt31To0);
		device->configSet(DVS132S_CONFIG_DVS, DVS132S_CONFIG_DVS_ROW_ENABLE_63_TO_32, rowInt63To32);
		device->configSet(DVS132S_CONFIG_DVS, DVS132S_CONFIG_DVS_ROW_ENABLE_65_TO_64, rowInt65To64);
	}

	static inline void dvsColumnEnableParse(const std::string &columnEnableStr, libcaer::devices::dvs132s *device) {
		size_t columnEnableIndex = 0;

		uint32_t columnInt31To0 = 0;

		for (size_t i = 0; i < 32; i++) {
			if (columnEnableStr[columnEnableIndex++] == '1') {
				columnInt31To0 |= static_cast<uint32_t>(0x01 << i);
			}
		}

		uint32_t columnInt51To32 = 0;

		for (size_t i = 0; i < 20; i++) {
			if (columnEnableStr[columnEnableIndex++] == '1') {
				columnInt51To32 |= static_cast<uint32_t>(0x01 << i);
			}
		}

		device->configSet(DVS132S_CONFIG_DVS, DVS132S_CONFIG_DVS_COLUMN_ENABLE_31_TO_0, columnInt31To0);
		device->configSet(DVS132S_CONFIG_DVS, DVS132S_CONFIG_DVS_COLUMN_ENABLE_51_TO_32, columnInt51To32);
	}

	static uint32_t mapAccelDataRate(const std::string &strVal) {
		if (strVal == "12.5 Hz") {
			return (BOSCH_ACCEL_12_5HZ);
		}
		else if (strVal == "25 Hz") {
			return (BOSCH_ACCEL_25HZ);
		}
		else if (strVal == "50 Hz") {
			return (BOSCH_ACCEL_50HZ);
		}
		else if (strVal == "100 Hz") {
			return (BOSCH_ACCEL_100HZ);
		}
		else if (strVal == "200 Hz") {
			return (BOSCH_ACCEL_200HZ);
		}
		else if (strVal == "400 Hz") {
			return (BOSCH_ACCEL_400HZ);
		}
		else if (strVal == "800 Hz") {
			return (BOSCH_ACCEL_800HZ);
		}
		else {
			return (BOSCH_ACCEL_1600HZ);
		}
	}

	static uint32_t mapAccelFilter(const std::string &strVal) {
		if (strVal == "Normal") {
			return (BOSCH_ACCEL_NORMAL);
		}
		else if (strVal == "OSR2") {
			return (BOSCH_ACCEL_OSR2);
		}
		else {
			return (BOSCH_ACCEL_OSR4);
		}
	}

	static uint32_t mapAccelRange(const std::string &strVal) {
		if (strVal == "±2G") {
			return (BOSCH_ACCEL_2G);
		}
		else if (strVal == "±4G") {
			return (BOSCH_ACCEL_4G);
		}
		else if (strVal == "±8G") {
			return (BOSCH_ACCEL_8G);
		}
		else {
			return (BOSCH_ACCEL_16G);
		}
	}

	static uint32_t mapGyroDataRate(const std::string &strVal) {
		if (strVal == "25 Hz") {
			return (BOSCH_GYRO_25HZ);
		}
		else if (strVal == "50 Hz") {
			return (BOSCH_GYRO_50HZ);
		}
		else if (strVal == "100 Hz") {
			return (BOSCH_GYRO_100HZ);
		}
		else if (strVal == "200 Hz") {
			return (BOSCH_GYRO_200HZ);
		}
		else if (strVal == "400 Hz") {
			return (BOSCH_GYRO_400HZ);
		}
		else if (strVal == "800 Hz") {
			return (BOSCH_GYRO_800HZ);
		}
		else if (strVal == "1600 Hz") {
			return (BOSCH_GYRO_1600HZ);
		}
		else {
			return (BOSCH_GYRO_3200HZ);
		}
	}

	static uint32_t mapGyroFilter(const std::string &strVal) {
		if (strVal == "Normal") {
			return (BOSCH_GYRO_NORMAL);
		}
		else if (strVal == "OSR2") {
			return (BOSCH_GYRO_OSR2);
		}
		else {
			return (BOSCH_GYRO_OSR4);
		}
	}

	static uint32_t mapGyroRange(const std::string &strVal) {
		if (strVal == "±125°/s") {
			return (BOSCH_GYRO_125DPS);
		}
		else if (strVal == "±250°/s") {
			return (BOSCH_GYRO_250DPS);
		}
		else if (strVal == "±500°/s") {
			return (BOSCH_GYRO_500DPS);
		}
		else if (strVal == "±1000°/s") {
			return (BOSCH_GYRO_1000DPS);
		}
		else {
			return (BOSCH_GYRO_2000DPS);
		}
	}
};

registerModuleClass(dvs132s)
