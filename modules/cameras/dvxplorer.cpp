#include "../../include/dv-sdk/data/event.hpp"
#include "../../include/dv-sdk/data/imu.hpp"
#include "../../include/dv-sdk/data/trigger.hpp"
#include "../../include/dv-sdk/module.hpp"

#include "../../src/log.hpp"
#include "aedat4_convert.hpp"

#include <libcaercpp/devices/device_discover.hpp>
#include <libcaercpp/devices/dvxplorer.hpp>

#include <chrono>

void *dvModuleGetHooks(enum dvModuleHooks hook) {
	// Support only device discovery.
	if (hook != DV_HOOK_DEVICE_DISCOVERY) {
		return nullptr;
	}

	// Get current device status via libcaer.
	// Suppress logging of libusb errors.
	libcaer::log::disable(true);

	auto devices = libcaer::devices::discover::device(CAER_DEVICE_DVXPLORER);

	libcaer::log::disable(false);

	// Add devices to config tree.
	auto devicesNode = dv::Cfg::GLOBAL.getNode("/system/_devices/");

	for (const auto &dev : devices) {
		const struct caer_dvx_info *info = &dev.deviceInfo.dvXplorerInfo;

		const auto nodeName = fmt::format(FMT_STRING("dvxplorer_{:d}-{:d}/"),
			static_cast<int>(info->deviceUSBBusNumber), static_cast<int>(info->deviceUSBDeviceAddress));

		auto devNode = devicesNode.getRelativeNode(nodeName);

		devNode.create<dv::CfgType::STRING>("OpenWithModule", "dv_dvxplorer", {1, 32},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Open device with specified module.");

		devNode.create<dv::CfgType::INT>("USBBusNumber", info->deviceUSBBusNumber, {0, 255},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "USB bus number.");
		devNode.create<dv::CfgType::INT>("USBDeviceAddress", info->deviceUSBDeviceAddress, {0, 255},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "USB device address.");

		if (!dev.deviceErrorOpen) {
			devNode.create<dv::CfgType::STRING>("SerialNumber", info->deviceSerialNumber, {0, 8},
				dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "USB device serial number.");
			devNode.create<dv::CfgType::INT>("FirmwareVersion", info->firmwareVersion, {0, INT16_MAX},
				dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Version of device firmware.");

			if (!dev.deviceErrorVersion) {
				devNode.create<dv::CfgType::INT>("LogicVersion", info->logicVersion, {0, INT16_MAX},
					dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Version of FPGA logic.");
				devNode.create<dv::CfgType::BOOL>("DeviceIsMaster", info->deviceIsMaster, {},
					dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Device is timestamp master.");

				devNode.create<dv::CfgType::INT>("DVSSizeX", info->dvsSizeX, {0, INT16_MAX},
					dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "DVS X axis resolution.");
				devNode.create<dv::CfgType::INT>("DVSSizeY", info->dvsSizeY, {0, INT16_MAX},
					dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "DVS Y axis resolution.");
			}
		}
	}

	// Nothing to return, no further steps needed.
	return nullptr;
}

class dvXplorer : public dv::ModuleBase {
private:
	libcaer::devices::dvXplorer device;
	bool isMipiCX3{false};

public:
	static void initOutputs(dv::OutputDefinitionList &out) {
		out.addEventOutput("events");
		out.addTriggerOutput("triggers");
		out.addIMUOutput("imu");
	}

	static const char *initDescription() {
		return ("iniVation DVXplorer camera support.");
	}

	static void initConfigOptions(dv::RuntimeConfig &config) {
		config.add("busNumber", dv::ConfigOption::intOption("USB bus number restriction.", 0, 0, UINT8_MAX));
		config.add("devAddress", dv::ConfigOption::intOption("USB device address restriction.", 0, 0, UINT8_MAX));
		config.add("serialNumber", dv::ConfigOption::stringOption("USB serial number restriction.", ""));

		// External trigger action.
		config.add("externalTriggerMode", dv::ConfigOption::listOption("External trigger reaction.", 0,
											  {"Reset Timestamps", "Single Frame Readout"}));

		// Simplified bias control.
		config.add("biasSensitivity",
			dv::ConfigOption::listOption("Configure contrast sensitivity via predefined values for current biases.",
				"Default", {"Very Low", "Low", "Default", "High", "Very High", "Custom"}));

		config.setPriorityOptions({"biasSensitivity"});

		subsampleConfigCreate(config);
		readoutConfigCreate(config);
		biasConfigCreate(config);
		multiplexerConfigCreate(config);
		imuConfigCreate(config);
		externalInputConfigCreate(config);
		usbConfigCreate(config);
		systemConfigCreate(config);
	}

	dvXplorer() :
		device(0, static_cast<uint8_t>(config.getInt("busNumber")), static_cast<uint8_t>(config.getInt("devAddress")),
			config.getString("serialNumber")) {
		// Initialize per-device log-level to module log-level.
		config.setString("logLevel", "WARNING");
		device.configSet(CAER_HOST_CONFIG_LOG, CAER_HOST_CONFIG_LOG_LEVEL,
			static_cast<uint32_t>(dv::LoggerInternal::logLevelNameToInteger(config.getString("logLevel"))));

		auto devInfo = device.infoGet();

		isMipiCX3 = (!devInfo.muxHasStatistics && !devInfo.dvsHasStatistics && !devInfo.extInputHasGenerator);

		// Generate source string for output modules.
		auto sourceString = std::string("DVXplorer_") + devInfo.deviceSerialNumber;

		// Setup outputs.
		outputs.getEventOutput("events").setup(device.infoGet().dvsSizeX, device.infoGet().dvsSizeY, sourceString);
		outputs.getTriggerOutput("triggers").setup(sourceString);
		outputs.getIMUOutput("imu").setup(sourceString);

		auto sourceInfoNode = moduleNode.getRelativeNode("sourceInfo/");

		sourceInfoNode.create<dv::CfgType::STRING>("serialNumber", devInfo.deviceSerialNumber, {0, 8},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Device serial number.");
		sourceInfoNode.create<dv::CfgType::INT>("usbBusNumber", devInfo.deviceUSBBusNumber, {0, 255},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Device USB bus number.");
		sourceInfoNode.create<dv::CfgType::INT>("usbDeviceAddress", devInfo.deviceUSBDeviceAddress, {0, 255},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Device USB device address.");

		sourceInfoNode.create<dv::CfgType::INT>("firmwareVersion", devInfo.firmwareVersion,
			{devInfo.firmwareVersion, devInfo.firmwareVersion}, dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT,
			"Device USB firmware version.");
		sourceInfoNode.create<dv::CfgType::INT>("logicVersion", devInfo.logicVersion,
			{devInfo.logicVersion, devInfo.logicVersion}, dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT,
			"Device FPGA logic version.");
		sourceInfoNode.create<dv::CfgType::INT>("chipID", devInfo.chipID, {devInfo.chipID, devInfo.chipID},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Device chip identification number.");

		// Extra features.
		sourceInfoNode.create<dv::CfgType::BOOL>("muxHasStatistics", devInfo.muxHasStatistics, {},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT,
			"Device supports FPGA Multiplexer statistics (USB event drops).");
		sourceInfoNode.create<dv::CfgType::BOOL>("dvsHasStatistics", devInfo.dvsHasStatistics, {},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Device supports FPGA DVS statistics.");
		sourceInfoNode.create<dv::CfgType::BOOL>("extInputHasGenerator", devInfo.extInputHasGenerator, {},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT,
			"Device supports generating pulses on output signal connector.");

		sourceInfoNode.create<dv::CfgType::BOOL>("deviceIsMaster", devInfo.deviceIsMaster, {},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT,
			"Timestamp synchronization support: device master status.");

		sourceInfoNode.create<dv::CfgType::STRING>("source", sourceString,
			{static_cast<int32_t>(sourceString.length()), static_cast<int32_t>(sourceString.length())},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Device source information.");

		try {
			// Ensure good defaults for data acquisition settings.
			// No blocking behavior due to mainloop notification, and no auto-start of
			// all producers to ensure cAER settings are respected.
			device.configSet(CAER_HOST_CONFIG_DATAEXCHANGE, CAER_HOST_CONFIG_DATAEXCHANGE_BLOCKING, true);
			device.configSet(CAER_HOST_CONFIG_DATAEXCHANGE, CAER_HOST_CONFIG_DATAEXCHANGE_START_PRODUCERS, true);
			device.configSet(CAER_HOST_CONFIG_DATAEXCHANGE, CAER_HOST_CONFIG_DATAEXCHANGE_STOP_PRODUCERS, true);

			// Default-initialize device.
			device.sendDefaultConfig();

			// Create default device-dependant settings.
			subsampleConfigCreateDynamic(&devInfo);
			cropConfigCreateDynamic(&devInfo);
			if (!isMipiCX3) {
				dvsStatisticsConfigCreateDynamic(&devInfo);
				multiplexerConfigCreateDynamic(&devInfo);
				externalInputConfigCreateDynamic(&devInfo);
			}

			// Set timestamp offset for real-time timestamps. DataStart() will
			// reset the device-side timestamp.
			int64_t tsNowOffset = std::chrono::duration_cast<std::chrono::microseconds>(
				std::chrono::system_clock::now().time_since_epoch())
									  .count();

			sourceInfoNode.create<dv::CfgType::LONG>("tsOffset", tsNowOffset, {0, INT64_MAX},
				dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT,
				"Time offset of data stream starting point to Unix time in µs.");

			moduleNode.getRelativeNode("outputs/events/info/")
				.create<dv::CfgType::LONG>("tsOffset", tsNowOffset, {0, INT64_MAX},
					dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT,
					"Time offset of data stream starting point to Unix time in µs.");

			moduleNode.getRelativeNode("outputs/triggers/info/")
				.create<dv::CfgType::LONG>("tsOffset", tsNowOffset, {0, INT64_MAX},
					dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT,
					"Time offset of data stream starting point to Unix time in µs.");

			moduleNode.getRelativeNode("outputs/imu/info/")
				.create<dv::CfgType::LONG>("tsOffset", tsNowOffset, {0, INT64_MAX},
					dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT,
					"Time offset of data stream starting point to Unix time in µs.");

			// Start data acquisition.
			device.dataStart(nullptr, nullptr, nullptr, &moduleShutdownNotify, moduleData->moduleNode);

			// Send all configuration to the device.
			sendDefaultConfiguration(&devInfo);
		}
		catch (const std::exception &ex) {
			// Remove statistics read modifiers.
			if (moduleNode.existsRelativeNode("statistics/")) {
				moduleNode.getRelativeNode("statistics/").attributeUpdaterRemoveAll();
			}

			// Clear sourceInfo node.
			auto sourceInfoNode = moduleNode.getRelativeNode("sourceInfo/");
			sourceInfoNode.removeAllAttributes();

			throw;
		}

		// Add config listeners last, to avoid having them dangling if Init doesn't succeed.
		moduleNode.getRelativeNode("subsample/").addAttributeListener(&device, &subsampleConfigListener);

		moduleNode.getRelativeNode("readout/").addAttributeListener(&device, &readoutConfigListener);

		moduleNode.getRelativeNode("bias/").addAttributeListener(&device, &biasConfigListener);

		moduleNode.getRelativeNode("crop/").addAttributeListener(&device, &cropConfigListener);

		moduleNode.getRelativeNode("imu/").addAttributeListener(&device, &imuConfigListener);

		if (!isMipiCX3) {
			moduleNode.getRelativeNode("multiplexer/").addAttributeListener(&device, &multiplexerConfigListener);

			moduleNode.getRelativeNode("externalInput/").addAttributeListener(&device, &externalInputConfigListener);

			moduleNode.getRelativeNode("usb/").addAttributeListener(&device, &usbConfigListener);
		}

		moduleNode.getRelativeNode("system/").addAttributeListener(&device, &systemConfigListener);
	}

	~dvXplorer() override {
		// Remove listener, which can reference invalid memory in userData.
		moduleNode.getRelativeNode("subsample/").removeAttributeListener(&device, &subsampleConfigListener);

		moduleNode.getRelativeNode("readout/").removeAttributeListener(&device, &readoutConfigListener);

		moduleNode.getRelativeNode("bias/").removeAttributeListener(&device, &biasConfigListener);

		moduleNode.getRelativeNode("crop/").removeAttributeListener(&device, &cropConfigListener);

		moduleNode.getRelativeNode("imu/").removeAttributeListener(&device, &imuConfigListener);

		if (!isMipiCX3) {
			moduleNode.getRelativeNode("multiplexer/").removeAttributeListener(&device, &multiplexerConfigListener);

			moduleNode.getRelativeNode("externalInput/").removeAttributeListener(&device, &externalInputConfigListener);

			moduleNode.getRelativeNode("usb/").removeAttributeListener(&device, &usbConfigListener);
		}

		moduleNode.getRelativeNode("system/").removeAttributeListener(&device, &systemConfigListener);

		// Stop data acquisition.
		device.dataStop();

		// Remove statistics read modifiers.
		if (moduleNode.existsRelativeNode("statistics/")) {
			moduleNode.getRelativeNode("statistics/").attributeUpdaterRemoveAll();
		}

		// Clear sourceInfo node.
		auto sourceInfoNode = moduleNode.getRelativeNode("sourceInfo/");
		sourceInfoNode.removeAllAttributes();
	}

	void run() override {
		auto data = device.dataGet();

		if (!data || data->empty()) {
			return;
		}

		if (data->getEventPacket(SPECIAL_EVENT)) {
			std::shared_ptr<const libcaer::events::SpecialEventPacket> special
				= std::static_pointer_cast<libcaer::events::SpecialEventPacket>(data->getEventPacket(SPECIAL_EVENT));

			if (special->getEventNumber() == 1 && (*special)[0].getType() == TIMESTAMP_RESET) {
				// Update master/slave information.
				auto devInfo = device.infoGet();

				auto sourceInfoNode = moduleNode.getRelativeNode("sourceInfo/");
				sourceInfoNode.updateReadOnly<dv::CfgType::BOOL>("deviceIsMaster", devInfo.deviceIsMaster);

				// Reset real-time timestamp offset.
				int64_t tsNowOffset = std::chrono::duration_cast<std::chrono::microseconds>(
					std::chrono::system_clock::now().time_since_epoch())
										  .count();

				sourceInfoNode.updateReadOnly<dv::CfgType::LONG>("tsOffset", tsNowOffset);

				moduleNode.getRelativeNode("outputs/events/info/")
					.updateReadOnly<dv::CfgType::LONG>("tsOffset", tsNowOffset);

				moduleNode.getRelativeNode("outputs/triggers/info/")
					.updateReadOnly<dv::CfgType::LONG>("tsOffset", tsNowOffset);

				moduleNode.getRelativeNode("outputs/imu/info/")
					.updateReadOnly<dv::CfgType::LONG>("tsOffset", tsNowOffset);
			}

			dvConvertToAedat4(special->getHeaderPointer(), moduleData);
		}

		if (data->size() == 1) {
			return;
		}

		if (data->getEventPacket(POLARITY_EVENT)) {
			dvConvertToAedat4(data->getEventPacket(POLARITY_EVENT)->getHeaderPointer(), moduleData);
		}

		// IMU6_EVENT is 2 here.
		if (data->getEventPacket(2)) {
			dvConvertToAedat4(data->getEventPacket(2)->getHeaderPointer(), moduleData);
		}
	}

	void configUpdate() override {
		device.configSet(CAER_HOST_CONFIG_LOG, CAER_HOST_CONFIG_LOG_LEVEL,
			static_cast<uint32_t>(dv::LoggerInternal::logLevelNameToInteger(config.getString("logLevel"))));

		auto externalTriggerMode = config.getString("externalTriggerMode");
		if (externalTriggerMode == "Single Frame Readout") {
			device.configSet(
				DVX_DVS_CHIP, DVX_DVS_CHIP_EXTERNAL_TRIGGER_MODE, DVX_DVS_CHIP_EXTERNAL_TRIGGER_MODE_SINGLE_FRAME);
		}
		else {
			device.configSet(
				DVX_DVS_CHIP, DVX_DVS_CHIP_EXTERNAL_TRIGGER_MODE, DVX_DVS_CHIP_EXTERNAL_TRIGGER_MODE_TIMESTAMP_RESET);
		}

		const auto biasSensitivity = config.getString("biasSensitivity");
		if (biasSensitivity != "Custom") {
			// Reset all biases to default.
			config.setInt("bias/BIAS_AMP", 4);

			config.setString("bias/CURRENT_RANGE_LOG", "5uA");
			config.setString("bias/CURRENT_RANGE_LOGA", "50uA");
			config.setString("bias/CURRENT_RANGE_LOGD", "50uA");
			config.setString("bias/CURRENT_RANGE_SF", "5uA");
			config.setString("bias/CURRENT_LEVEL_SF", "x1");
			config.setString("bias/CURRENT_RANGE_nRST", "0.5uA");

			// Setup appropriate contrast threshold.
			if (biasSensitivity == "Very Low") {
				config.setInt("bias/BIAS_ON", 15);
				config.setInt("bias/BIAS_OFF", 15);
			}
			else if (biasSensitivity == "Low") {
				config.setInt("bias/BIAS_ON", 12);
				config.setInt("bias/BIAS_OFF", 12);
			}
			else if (biasSensitivity == "High") {
				config.setInt("bias/BIAS_ON", 5);
				config.setInt("bias/BIAS_OFF", 5);
			}
			else if (biasSensitivity == "Very High") {
				config.setInt("bias/BIAS_ON", 2);
				config.setInt("bias/BIAS_OFF", 2);
			}
			else {
				// Default bias sensitivity (middle contrast threshold).
				config.setInt("bias/BIAS_ON", 9);
				config.setInt("bias/BIAS_OFF", 9);
			}
		}
	}

private:
	static void moduleShutdownNotify(void *p) {
		dv::Cfg::Node moduleNode = static_cast<dvConfigNode>(p);

		// Ensure parent also shuts down (on disconnected device for example).
		moduleNode.putBool("running", false);
	}

	void sendDefaultConfiguration(const struct caer_dvx_info *devInfo) {
		systemConfigSend();

		if (!isMipiCX3) {
			usbConfigSend();
			multiplexerConfigSend();

			// Wait 50 ms for data transfer to be ready.
			struct timespec noDataSleep = {.tv_sec = 0, .tv_nsec = 50000000};
			nanosleep(&noDataSleep, nullptr);

			externalInputConfigSend(devInfo);
		}

		subsampleConfigSend(devInfo);
		biasConfigSend();
		readoutConfigSend();
		cropConfigSend();
		imuConfigSend();
	}

	static void subsampleConfigCreate(dv::RuntimeConfig &config) {
		config.add("subsample/EventsFlatten", dv::ConfigOption::boolOption("Flatten events to all be ON events."));
		config.add("subsample/EventsOnOnly", dv::ConfigOption::boolOption("Only generate ON events."));
		config.add("subsample/EventsOffOnly", dv::ConfigOption::boolOption("Only generate OFF events."));

		config.add("subsample/Enable", dv::ConfigOption::boolOption("Enable sub-sampling of events."));

		// TODO: add area blocking.
		// config.add("areaBlocking", dv::ConfigOption::boolOption("Enable blocking off 32x32 patches of events."));
		// config.add("areaBlockingSelection", dv::ConfigOption::stringOption("Block off 32x32 patches of
		// events.","0000"));

		config.setPriorityOptions({"subsample/"});
	}

	void subsampleConfigCreateDynamic(const struct caer_dvx_info *devInfo) {
		if (devInfo->chipID == DVXPLORER_CHIP_ID) {
			config.add("subsample/Horizontal",
				dv::ConfigOption::listOption("Horizontal sub-sampling factor.", 0, {"none", "1/2", "1/4", "1/8"}));
			config.add("subsample/Vertical",
				dv::ConfigOption::listOption("Vertical sub-sampling factor.", 0, {"none", "1/2", "1/4", "1/8"}));
			config.add("subsample/DualBinning", dv::ConfigOption::boolOption("Enable dual-binning of events."));
		}
		else {
			config.add("subsample/Horizontal",
				dv::ConfigOption::listOption("Horizontal sub-sampling factor.", 0, {"none", "1/2", "1/4"}));
			config.add("subsample/Vertical",
				dv::ConfigOption::listOption("Vertical sub-sampling factor.", 0, {"none", "1/2", "1/4"}));
		}
	}

	void subsampleConfigSend(const struct caer_dvx_info *devInfo) {
		device.configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_EVENT_FLATTEN, config.getBool("subsample/EventsFlatten"));
		device.configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_EVENT_ON_ONLY, config.getBool("subsample/EventsOnOnly"));
		device.configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_EVENT_OFF_ONLY, config.getBool("subsample/EventsOffOnly"));

		device.configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_SUBSAMPLE_ENABLE, config.getBool("subsample/Enable"));

		auto subsampleHorizontal = config.getString("subsample/Horizontal");
		device.configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_SUBSAMPLE_HORIZONTAL, mapSubsampleFactor(subsampleHorizontal));

		auto subsampleVertical = config.getString("subsample/Vertical");
		device.configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_SUBSAMPLE_VERTICAL, mapSubsampleFactor(subsampleVertical));

		if (devInfo->chipID == DVXPLORER_CHIP_ID) {
			device.configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_DUAL_BINNING_ENABLE, config.getBool("subsample/DualBinning"));
		}

		// TODO: add area blocking.
	}

	static void subsampleConfigListener(dvConfigNode node, void *userData, enum dvConfigAttributeEvents event,
		const char *changeKey, enum dvConfigAttributeType changeType, union dvConfigAttributeValue changeValue) {
		UNUSED_ARGUMENT(node);

		auto device = static_cast<libcaer::devices::dvXplorer *>(userData);

		std::string key{changeKey};

		if (event == DVCFG_ATTRIBUTE_MODIFIED) {
			if (changeType == DVCFG_TYPE_BOOL && key == "EventsFlatten") {
				device->configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_EVENT_FLATTEN, changeValue.boolean);
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "EventsOnOnly") {
				device->configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_EVENT_ON_ONLY, changeValue.boolean);
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "EventsOffOnly") {
				device->configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_EVENT_OFF_ONLY, changeValue.boolean);
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "Enable") {
				device->configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_SUBSAMPLE_ENABLE, changeValue.boolean);
			}
			else if (changeType == DVCFG_TYPE_STRING && key == "Horizontal") {
				device->configSet(
					DVX_DVS_CHIP, DVX_DVS_CHIP_SUBSAMPLE_HORIZONTAL, mapSubsampleFactor(changeValue.string));
			}
			else if (changeType == DVCFG_TYPE_STRING && key == "Vertical") {
				device->configSet(
					DVX_DVS_CHIP, DVX_DVS_CHIP_SUBSAMPLE_VERTICAL, mapSubsampleFactor(changeValue.string));
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "DualBinning") {
				device->configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_DUAL_BINNING_ENABLE, changeValue.boolean);
			}

			// TODO: add area blocking.
		}
	}

	static void biasConfigCreate(dv::RuntimeConfig &config) {
		// Biases LOG stage.
		config.add(
			"bias/CURRENT_RANGE_LOG", dv::ConfigOption::listOption("Bias CURRENT_RANGE_LOG.", "5uA", {"5uA", "50uA"}));
		config.add("bias/CURRENT_RANGE_LOGA",
			dv::ConfigOption::listOption("Bias CURRENT_RANGE_LOGA.", "50uA", {"5uA", "50uA"}));
		config.add("bias/CURRENT_RANGE_LOGD",
			dv::ConfigOption::listOption("Bias CURRENT_RANGE_LOGD.", "50uA", {"5uA", "50uA", "500uA"}));

		// Biases SF stage.
		config.add(
			"bias/CURRENT_RANGE_SF", dv::ConfigOption::listOption("Bias CURRENT_RANGE_SF.", "5uA", {"0.5uA", "5uA"}));
		config.add(
			"bias/CURRENT_LEVEL_SF", dv::ConfigOption::listOption("Bias CURRENT_LEVEL_SF.", "x1", {"x0.1", "x1"}));

		// Biases CFA stage.
		config.add("bias/CURRENT_RANGE_nRST",
			dv::ConfigOption::listOption("Bias CURRENT_RANGE_nRST.", "0.5uA", {"0.5uA", "5uA"}));
		config.add("bias/BIAS_AMP", dv::ConfigOption::intOption("Bias BIAS_AMP.", 4, 0, 8));

		// Biases COMPARATOR stage.
		config.add("bias/BIAS_ON", dv::ConfigOption::intOption("Bias BIAS_ON.", 9, 0, 17));
		config.add("bias/BIAS_OFF", dv::ConfigOption::intOption("Bias BIAS_OFF.", 9, 0, 17));

		config.setPriorityOptions({"bias/"});
	}

	static void parseBiasRanges(
		const std::string_view biasName, const std::string_view biasValue, libcaer::devices::dvXplorer &handle) {
		if (biasName == "CURRENT_RANGE_LOG") {
			handle.configSet(DVX_DVS_CHIP_BIAS, DVX_DVS_CHIP_BIAS_CURRENT_RANGE_LOG, (biasValue == "50uA"));
		}
		else if (biasName == "CURRENT_RANGE_LOGA") {
			handle.configSet(DVX_DVS_CHIP_BIAS, DVX_DVS_CHIP_BIAS_CURRENT_RANGE_LOGA, (biasValue == "50uA"));
		}
		else if (biasName == "CURRENT_RANGE_LOGD") {
			handle.configSet(DVX_DVS_CHIP_BIAS, DVX_DVS_CHIP_BIAS_CURRENT_RANGE_LOGD,
				(biasValue == "500uA") ? (2) : (biasValue == "50uA"));
		}
		else if (biasName == "CURRENT_RANGE_SF") {
			handle.configSet(DVX_DVS_CHIP_BIAS, DVX_DVS_CHIP_BIAS_CURRENT_RANGE_SF, (biasValue == "5uA"));
		}
		else if (biasName == "CURRENT_LEVEL_SF") {
			handle.configSet(DVX_DVS_CHIP_BIAS, DVX_DVS_CHIP_BIAS_CURRENT_LEVEL_SF, (biasValue == "x1"));
		}
		else if (biasName == "CURRENT_RANGE_nRST") {
			handle.configSet(DVX_DVS_CHIP_BIAS, DVX_DVS_CHIP_BIAS_CURRENT_RANGE_nRST, (biasValue == "5uA"));
		}
	}

	static void parseBiasOnOff(const bool biasOn, const uint32_t biasValue, libcaer::devices::dvXplorer &handle) {
		if (biasOn) {
			// ON BIAS.
			if (biasValue < 9) {
				handle.configSet(
					DVX_DVS_CHIP_BIAS, DVX_DVS_CHIP_BIAS_CURRENT_RANGE_ON, DVX_DVS_CHIP_BIAS_CURRENT_RANGE_ON_5uA);
				handle.configSet(DVX_DVS_CHIP_BIAS, DVX_DVS_CHIP_BIAS_CURRENT_ON, biasValue);
			}
			else {
				handle.configSet(
					DVX_DVS_CHIP_BIAS, DVX_DVS_CHIP_BIAS_CURRENT_RANGE_ON, DVX_DVS_CHIP_BIAS_CURRENT_RANGE_ON_50uA);
				handle.configSet(DVX_DVS_CHIP_BIAS, DVX_DVS_CHIP_BIAS_CURRENT_ON, biasValue - 9);
			}
		}
		else {
			// OFF BIAS.
			if (biasValue < 9) {
				handle.configSet(
					DVX_DVS_CHIP_BIAS, DVX_DVS_CHIP_BIAS_CURRENT_LEVEL_nOFF, DVX_DVS_CHIP_BIAS_CURRENT_LEVEL_nOFF_x1);
				handle.configSet(DVX_DVS_CHIP_BIAS, DVX_DVS_CHIP_BIAS_CURRENT_OFF, 8 - biasValue);
			}
			else {
				handle.configSet(
					DVX_DVS_CHIP_BIAS, DVX_DVS_CHIP_BIAS_CURRENT_LEVEL_nOFF, DVX_DVS_CHIP_BIAS_CURRENT_LEVEL_nOFF_x0_1);
				handle.configSet(DVX_DVS_CHIP_BIAS, DVX_DVS_CHIP_BIAS_CURRENT_OFF, 8 - (biasValue - 9));
			}
		}
	}

	void biasConfigSend() {
		device.configSet(DVX_DVS_CHIP_BIAS, DVX_DVS_CHIP_BIAS_CURRENT_AMP, config.getInt("bias/BIAS_AMP"));

		parseBiasOnOff(true, config.getInt("bias/BIAS_ON"), device);
		parseBiasOnOff(false, config.getInt("bias/BIAS_OFF"), device);

		parseBiasRanges("CURRENT_RANGE_LOG", config.getString("bias/CURRENT_RANGE_LOG"), device);
		parseBiasRanges("CURRENT_RANGE_LOGA", config.getString("bias/CURRENT_RANGE_LOGA"), device);
		parseBiasRanges("CURRENT_RANGE_LOGD", config.getString("bias/CURRENT_RANGE_LOGD"), device);
		parseBiasRanges("CURRENT_RANGE_SF", config.getString("bias/CURRENT_RANGE_SF"), device);
		parseBiasRanges("CURRENT_LEVEL_SF", config.getString("bias/CURRENT_LEVEL_SF"), device);
		parseBiasRanges("CURRENT_RANGE_nRST", config.getString("bias/CURRENT_RANGE_nRST"), device);
	}

	static void biasConfigListener(dvConfigNode node, void *userData, enum dvConfigAttributeEvents event,
		const char *changeKey, enum dvConfigAttributeType changeType, union dvConfigAttributeValue changeValue) {
		UNUSED_ARGUMENT(node);

		auto device = static_cast<libcaer::devices::dvXplorer *>(userData);

		std::string key{changeKey};

		if (event == DVCFG_ATTRIBUTE_MODIFIED) {
			if (changeType == DVCFG_TYPE_INT && key == "BIAS_AMP") {
				device->configSet(DVX_DVS_CHIP_BIAS, DVX_DVS_CHIP_BIAS_CURRENT_AMP, changeValue.iint);
			}
			else if (changeType == DVCFG_TYPE_INT) {
				parseBiasOnOff((key == "BIAS_ON"), changeValue.iint, *device);
			}
			else if (changeType == DVCFG_TYPE_STRING) {
				parseBiasRanges(key, changeValue.string, *device);
			}
		}
	}

	static void readoutConfigCreate(dv::RuntimeConfig &config) {
		config.add("readout/GlobalReset", dv::ConfigOption::boolOption("Enable global reset.", false));
		config.add("readout/GlobalHold", dv::ConfigOption::boolOption("Enable global hold.", true));

		// Predefined readout timing options.
		config.add(
			"readout/eFPS", dv::ConfigOption::listOption("Maximum number of event-frames per second (readout speed).",
								"5000 (variable)",
								{"100 (constant)", "200 (constant)", "500 (constant)", "1000 (constant)",
									"2000 (constant lossy)", "5000 (constant lossy)", "10000 (constant lossy)",
									"2000 (variable)", "5000 (variable)", "10000 (variable)", "15000 (variable)"}));

		config.setPriorityOptions({"readout/eFPS"});
	}

	static void setReadoutTiming(const std::string_view eFPS, libcaer::devices::dvXplorer &handle) {
		// Changes to readout timings need to happen while chip is not reading out data, to avoid lock-up.
		handle.configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_DTAG_CONTROL, DVX_DVS_CHIP_DTAG_CONTROL_STOP);

		if (eFPS.ends_with("(constant)")) {
			// Worst case readout time is around 0.85ms, so if we want 1k FPS or slower we need to increase T_ED
			// to compensate, and at this point it makes sense to also make the timing constant, using the
			// fixed read time readout mode. Default setting is 45000 cycles @ 50 MHz: 900ms.
			handle.configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_TIMING_READ_FIXED, 45000);
			handle.configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_FIXED_READ_TIME_ENABLE, true);

			handle.configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_TIMING_NEXT_SEL, 15); // Safe default.

			if (eFPS == "100 (constant)") {
				// 10000ms per frame: 10000-900=9100
				handle.configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_TIMING_ED, 9100);
			}
			else if (eFPS == "200 (constant)") {
				// 5000ms per frame: 5000-900=4100
				handle.configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_TIMING_ED, 4100);
			}
			else if (eFPS == "500 (constant)") {
				// 2000ms per frame: 2000-900=1100
				handle.configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_TIMING_ED, 1100);
			}
			else if (eFPS == "1000 (constant)") {
				// 1000ms per frame: 1000-900=100
				handle.configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_TIMING_ED, 100);
			}
		}
		else if (eFPS.ends_with("(constant lossy)")) {
			// Constant readout time can also be used at values below the maximum readout time, you then simply
			// hit possible data loss if the readout cannot complete in time. It's still a useful option to have.
			// Start by minimizing T_ED and readout time as much as possible, to maximize the chance of it finishing
			// in the allotted timeframe.
			handle.configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_FIXED_READ_TIME_ENABLE, true);
			handle.configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_TIMING_ED, 1);       // 1µs T_ED, minimum safe value.
			handle.configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_TIMING_NEXT_SEL, 5); // Smallest NEXT_SEL value.

			if (eFPS == "2000 (constant lossy)") {
				// 500 µs per frame: 50 x 500 = 25000
				handle.configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_TIMING_READ_FIXED, 25000);
			}
			else if (eFPS == "5000 (constant lossy)") {
				// 200 µs per frame: 50 x 200 = 10000
				handle.configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_TIMING_READ_FIXED, 10000);
			}
			else if (eFPS == "10000 (constant lossy)") {
				// 100 µs per frame: 50 x 100 = 5000
				handle.configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_TIMING_READ_FIXED, 5000);
			}
		}
		else {
			// For high-speed, variable time readout mode is used (less events, more speed).
			handle.configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_FIXED_READ_TIME_ENABLE, false);

			if (eFPS == "2000 (variable)") {
				// 307+192=499µs min readout time.
				handle.configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_TIMING_ED, 307);      // 307µs T_ED.
				handle.configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_TIMING_NEXT_SEL, 15); // Safe default.
			}
			else if (eFPS == "5000 (variable)") {
				// 7+192=199µs min readout time.
				handle.configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_TIMING_ED, 7);        // 7µs T_ED.
				handle.configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_TIMING_NEXT_SEL, 15); // Safe default.
			}
			else if (eFPS == "10000 (variable)") {
				// 10+89=99µs min readout time.
				handle.configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_TIMING_ED, 10);      // 10µs T_ED.
				handle.configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_TIMING_NEXT_SEL, 7); // Second smallest NEXT_SEL value.
			}
			else if (eFPS == "15000 (variable)") {
				// 1+64=65µs min readout time.
				handle.configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_TIMING_ED, 1);       // 1µs T_ED, minimum safe value.
				handle.configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_TIMING_NEXT_SEL, 5); // Smallest NEXT_SEL value.
			}
		}

		handle.configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_DTAG_CONTROL, DVX_DVS_CHIP_DTAG_CONTROL_START);
	}

	void readoutConfigSend() {
		device.configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_GLOBAL_RESET_ENABLE, config.getBool("readout/GlobalReset"));
		device.configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_GLOBAL_HOLD_ENABLE, config.getBool("readout/GlobalHold"));

		setReadoutTiming(config.getString("readout/eFPS"), device);
	}

	static void readoutConfigListener(dvConfigNode node, void *userData, enum dvConfigAttributeEvents event,
		const char *changeKey, enum dvConfigAttributeType changeType, union dvConfigAttributeValue changeValue) {
		UNUSED_ARGUMENT(node);

		auto device = static_cast<libcaer::devices::dvXplorer *>(userData);

		std::string key{changeKey};

		if (event == DVCFG_ATTRIBUTE_MODIFIED) {
			if (changeType == DVCFG_TYPE_BOOL && key == "GlobalReset") {
				device->configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_GLOBAL_RESET_ENABLE, changeValue.boolean);
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "GlobalHold") {
				device->configSet(DVX_DVS_CHIP, DVX_DVS_CHIP_GLOBAL_HOLD_ENABLE, changeValue.boolean);
			}
			else if (changeType == DVCFG_TYPE_STRING && key == "eFPS") {
				setReadoutTiming(changeValue.string, *device);
			}
		}
	}

	void cropConfigCreateDynamic(const struct caer_dvx_info *devInfo) {
		// Add size-dependant configuration.
		config.add("crop/Enable", dv::ConfigOption::boolOption("Enable Region of Interest (ROI)."));
		config.add("crop/StartX",
			dv::ConfigOption::intOption("Horizontal (X axis) start of ROI.", 0, 0, devInfo->dvsSizeX - 1));
		config.add(
			"crop/StartY", dv::ConfigOption::intOption("Vertical (Y axis) start of ROI.", 0, 0, devInfo->dvsSizeY - 1));
		config.add("crop/EndX", dv::ConfigOption::intOption("Horizontal (X axis) end of ROI.", devInfo->dvsSizeX - 1, 0,
									devInfo->dvsSizeX - 1));
		config.add("crop/EndY", dv::ConfigOption::intOption(
									"Vertical (Y axis) end of ROI.", devInfo->dvsSizeY - 1, 0, devInfo->dvsSizeY - 1));

		config.setPriorityOptions({"crop/"});
	}

	void cropConfigSend() {
		device.configSet(DVX_DVS_CHIP_CROPPER, DVX_DVS_CHIP_CROPPER_ENABLE, config.getBool("crop/Enable"));
		device.configSet(DVX_DVS_CHIP_CROPPER, DVX_DVS_CHIP_CROPPER_X_START_ADDRESS,
			static_cast<uint32_t>(config.getInt("crop/StartX")));
		device.configSet(DVX_DVS_CHIP_CROPPER, DVX_DVS_CHIP_CROPPER_Y_START_ADDRESS,
			static_cast<uint32_t>(config.getInt("crop/StartY")));
		device.configSet(DVX_DVS_CHIP_CROPPER, DVX_DVS_CHIP_CROPPER_X_END_ADDRESS,
			static_cast<uint32_t>(config.getInt("crop/EndX")));
		device.configSet(DVX_DVS_CHIP_CROPPER, DVX_DVS_CHIP_CROPPER_Y_END_ADDRESS,
			static_cast<uint32_t>(config.getInt("crop/EndY")));
	}

	static void cropConfigListener(dvConfigNode node, void *userData, enum dvConfigAttributeEvents event,
		const char *changeKey, enum dvConfigAttributeType changeType, union dvConfigAttributeValue changeValue) {
		UNUSED_ARGUMENT(node);

		auto device = static_cast<libcaer::devices::dvXplorer *>(userData);

		std::string key{changeKey};

		if (event == DVCFG_ATTRIBUTE_MODIFIED) {
			if (changeType == DVCFG_TYPE_BOOL && key == "Enable") {
				device->configSet(DVX_DVS_CHIP_CROPPER, DVX_DVS_CHIP_CROPPER_ENABLE, changeValue.boolean);
			}
			else if (changeType == DVCFG_TYPE_INT && key == "StartX") {
				device->configSet(DVX_DVS_CHIP_CROPPER, DVX_DVS_CHIP_CROPPER_X_START_ADDRESS,
					static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "StartY") {
				device->configSet(DVX_DVS_CHIP_CROPPER, DVX_DVS_CHIP_CROPPER_Y_START_ADDRESS,
					static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "EndX") {
				device->configSet(
					DVX_DVS_CHIP_CROPPER, DVX_DVS_CHIP_CROPPER_X_END_ADDRESS, static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "EndY") {
				device->configSet(
					DVX_DVS_CHIP_CROPPER, DVX_DVS_CHIP_CROPPER_Y_END_ADDRESS, static_cast<uint32_t>(changeValue.iint));
			}
		}
	}

	void dvsStatisticsConfigCreateDynamic(const struct caer_dvx_info *devInfo) {
		// Device event statistics.
		if (devInfo->dvsHasStatistics) {
			config.add("statistics/dvsColumns", dv::ConfigOption::statisticOption("Number of read DVS column events."));
			config.add(
				"statistics/dvsGroups", dv::ConfigOption::statisticOption("Number of read DVS row group events."));
			config.add("statistics/dvsDroppedColumns",
				dv::ConfigOption::statisticOption("Number of dropped DVS column events due to USB full."));
			config.add("statistics/dvsDroppedGroups",
				dv::ConfigOption::statisticOption("Number of dropped DVS row group events due to USB full."));

			auto statNode = moduleNode.getRelativeNode("statistics/");

			statNode.attributeUpdaterAdd("dvsColumns", dv::CfgType::LONG, &statisticsUpdater, &device);
			statNode.attributeUpdaterAdd("dvsGroups", dv::CfgType::LONG, &statisticsUpdater, &device);
			statNode.attributeUpdaterAdd("dvsDroppedColumns", dv::CfgType::LONG, &statisticsUpdater, &device);
			statNode.attributeUpdaterAdd("dvsDroppedGroups", dv::CfgType::LONG, &statisticsUpdater, &device);

			config.setPriorityOptions({"statistics/"});
		}
	}

	static void multiplexerConfigCreate(dv::RuntimeConfig &config) {
		// Subsystem 0: Multiplexer
		config.add("multiplexer/TimestampReset",
			dv::ConfigOption::buttonOption("Reset timestamps to zero.", "Reset timestamps"));
		config.add("multiplexer/DropDVSOnTransferStall",
			dv::ConfigOption::boolOption("Drop Polarity events when USB FIFO is full.", false));
		config.add("multiplexer/DropExtInputOnTransferStall",
			dv::ConfigOption::boolOption("Drop ExternalInput events when USB FIFO is full.", true));

		config.setPriorityOptions({"multiplexer/"});
	}

	void multiplexerConfigCreateDynamic(const struct caer_dvx_info *devInfo) {
		// Device event statistics.
		if (devInfo->muxHasStatistics) {
			config.add("statistics/muxDroppedDVS",
				dv::ConfigOption::statisticOption("Number of dropped DVS events due to USB full."));
			config.add("statistics/muxDroppedExtInput",
				dv::ConfigOption::statisticOption("Number of dropped External Input events due to USB full."));

			auto statNode = moduleNode.getRelativeNode("statistics/");

			statNode.attributeUpdaterAdd("muxDroppedDVS", dv::CfgType::LONG, &statisticsUpdater, &device);
			statNode.attributeUpdaterAdd("muxDroppedExtInput", dv::CfgType::LONG, &statisticsUpdater, &device);

			config.setPriorityOptions({"statistics/"});
		}
	}

	void multiplexerConfigSend() {
		device.configSet(DVX_MUX, DVX_MUX_TIMESTAMP_RESET, false);
		config.setBool("multiplexer/TimestampReset", false); // Ensure default value is reset.
		device.configSet(
			DVX_MUX, DVX_MUX_DROP_DVS_ON_TRANSFER_STALL, config.getBool("multiplexer/DropDVSOnTransferStall"));
		device.configSet(DVX_MUX, DVX_MUX_DROP_EXTINPUT_ON_TRANSFER_STALL,
			config.getBool("multiplexer/DropExtInputOnTransferStall"));
	}

	static void multiplexerConfigListener(dvConfigNode node, void *userData, enum dvConfigAttributeEvents event,
		const char *changeKey, enum dvConfigAttributeType changeType, union dvConfigAttributeValue changeValue) {
		UNUSED_ARGUMENT(node);

		auto device = static_cast<libcaer::devices::dvXplorer *>(userData);

		std::string key{changeKey};

		if (event == DVCFG_ATTRIBUTE_MODIFIED) {
			if (changeType == DVCFG_TYPE_BOOL && key == "TimestampReset" && changeValue.boolean) {
				device->configSet(DVX_MUX, DVX_MUX_TIMESTAMP_RESET, changeValue.boolean);

				dvConfigNodeAttributeBooleanReset(node, changeKey);
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "DropDVSOnTransferStall") {
				device->configSet(DVX_MUX, DVX_MUX_DROP_DVS_ON_TRANSFER_STALL, changeValue.boolean);
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "DropExtInputOnTransferStall") {
				device->configSet(DVX_MUX, DVX_MUX_DROP_EXTINPUT_ON_TRANSFER_STALL, changeValue.boolean);
			}
		}
	}

	static void imuConfigCreate(dv::RuntimeConfig &config) {
		// Subsystem 3: IMU
		config.add("imu/RunAccelerometer", dv::ConfigOption::boolOption("Enable accelerometer.", true));
		config.add("imu/RunGyroscope", dv::ConfigOption::boolOption("Enable gyroscope.", true));
		config.add("imu/RunTemperature", dv::ConfigOption::boolOption("Enable temperature sensor.", true));
		config.add(
			"imu/AccelDataRate", dv::ConfigOption::listOption("Accelerometer bandwidth configuration.", "800 Hz",
									 {"12.5 Hz", "25 Hz", "50 Hz", "100 Hz", "200 Hz", "400 Hz", "800 Hz", "1600 Hz"}));
		config.add("imu/AccelFilter",
			dv::ConfigOption::listOption("Accelerometer filter configuration.", "Normal", {"Normal", "OSR2", "OSR4"}));
		config.add("imu/AccelRange",
			dv::ConfigOption::listOption("Accelerometer range configuration.", "±4G", {"±2G", "±4G", "±8G", "±16G"}));
		config.add(
			"imu/GyroDataRate", dv::ConfigOption::listOption("Gyroscope bandwidth configuration.", "800 Hz",
									{"25 Hz", "50 Hz", "100 Hz", "200 Hz", "400 Hz", "800 Hz", "1600 Hz", "3200 Hz"}));
		config.add("imu/GyroFilter",
			dv::ConfigOption::listOption("Gyroscope filter configuration.", "Normal", {"Normal", "OSR2", "OSR4"}));
		config.add("imu/GyroRange", dv::ConfigOption::listOption("Gyroscope range configuration.", "±500°/s",
										{"±125°/s", "±250°/s", "±500°/s", "±1000°/s", "±2000°/s"}));

		config.setPriorityOptions({"imu/RunAccelerometer", "imu/RunGyroscope"});
	}

	void imuConfigSend() {
		device.configSet(DVX_IMU, DVX_IMU_ACCEL_DATA_RATE, mapAccelDataRate(config.getString("imu/AccelDataRate")));
		device.configSet(DVX_IMU, DVX_IMU_ACCEL_FILTER, mapAccelFilter(config.getString("imu/AccelFilter")));
		device.configSet(DVX_IMU, DVX_IMU_ACCEL_RANGE, mapAccelRange(config.getString("imu/AccelRange")));
		device.configSet(DVX_IMU, DVX_IMU_GYRO_DATA_RATE, mapGyroDataRate(config.getString("imu/GyroDataRate")));
		device.configSet(DVX_IMU, DVX_IMU_GYRO_FILTER, mapGyroFilter(config.getString("imu/GyroFilter")));
		device.configSet(DVX_IMU, DVX_IMU_GYRO_RANGE, mapGyroRange(config.getString("imu/GyroRange")));

		device.configSet(DVX_IMU, DVX_IMU_RUN_ACCELEROMETER, config.getBool("imu/RunAccelerometer"));
		device.configSet(DVX_IMU, DVX_IMU_RUN_GYROSCOPE, config.getBool("imu/RunGyroscope"));
		device.configSet(DVX_IMU, DVX_IMU_RUN_TEMPERATURE, config.getBool("imu/RunTemperature"));
	}

	static void imuConfigListener(dvConfigNode node, void *userData, enum dvConfigAttributeEvents event,
		const char *changeKey, enum dvConfigAttributeType changeType, union dvConfigAttributeValue changeValue) {
		UNUSED_ARGUMENT(node);

		auto device = static_cast<libcaer::devices::dvXplorer *>(userData);

		std::string key{changeKey};

		if (event == DVCFG_ATTRIBUTE_MODIFIED) {
			if (changeType == DVCFG_TYPE_STRING && key == "AccelDataRate") {
				device->configSet(DVX_IMU, DVX_IMU_ACCEL_DATA_RATE, mapAccelDataRate(changeValue.string));
			}
			else if (changeType == DVCFG_TYPE_STRING && key == "AccelFilter") {
				device->configSet(DVX_IMU, DVX_IMU_ACCEL_FILTER, mapAccelFilter(changeValue.string));
			}
			else if (changeType == DVCFG_TYPE_STRING && key == "AccelRange") {
				device->configSet(DVX_IMU, DVX_IMU_ACCEL_RANGE, mapAccelRange(changeValue.string));
			}
			else if (changeType == DVCFG_TYPE_STRING && key == "GyroDataRate") {
				device->configSet(DVX_IMU, DVX_IMU_GYRO_DATA_RATE, mapGyroDataRate(changeValue.string));
			}
			else if (changeType == DVCFG_TYPE_STRING && key == "GyroFilter") {
				device->configSet(DVX_IMU, DVX_IMU_GYRO_FILTER, mapGyroFilter(changeValue.string));
			}
			else if (changeType == DVCFG_TYPE_STRING && key == "GyroRange") {
				device->configSet(DVX_IMU, DVX_IMU_GYRO_RANGE, mapGyroRange(changeValue.string));
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "RunAccelerometer") {
				device->configSet(DVX_IMU, DVX_IMU_RUN_ACCELEROMETER, changeValue.boolean);
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "RunGyroscope") {
				device->configSet(DVX_IMU, DVX_IMU_RUN_GYROSCOPE, changeValue.boolean);
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "RunTemperature") {
				device->configSet(DVX_IMU, DVX_IMU_RUN_TEMPERATURE, changeValue.boolean);
			}
		}
	}

	static void externalInputConfigCreate(dv::RuntimeConfig &config) {
		// Subsystem 4: External Input
		config.add("externalInput/RunDetector", dv::ConfigOption::boolOption("Enable signal detector."));
		config.add("externalInput/DetectRisingEdges",
			dv::ConfigOption::boolOption("Emit special event if a rising edge is detected."));
		config.add("externalInput/DetectFallingEdges",
			dv::ConfigOption::boolOption("Emit special event if a falling edge is detected."));
		config.add(
			"externalInput/DetectPulses", dv::ConfigOption::boolOption("Emit special event if a pulse is detected."));
		config.add("externalInput/DetectPulsePolarity",
			dv::ConfigOption::boolOption("Polarity of the pulse to be detected.", true));
		config.add("externalInput/DetectPulseLength",
			dv::ConfigOption::intOption(
				"Minimal length of the pulse to be detected (in µs).", 10, 1, ((0x01 << 20) - 1)));

		config.setPriorityOptions({"externalInput/"});
	}

	void externalInputConfigCreateDynamic(const struct caer_dvx_info *devInfo) {
		if (devInfo->extInputHasGenerator) {
			config.add(
				"externalInput/RunGenerator", dv::ConfigOption::boolOption("Enable signal generator (PWM-like)."));
			config.add("externalInput/GeneratePulsePolarity",
				dv::ConfigOption::boolOption("Polarity of the generated pulse.", true));
			config.add("externalInput/GeneratePulseInterval",
				dv::ConfigOption::intOption(
					"Time interval between consecutive pulses (in µs).", 10, 1, ((0x01 << 20) - 1)));
			config.add("externalInput/GeneratePulseLength",
				dv::ConfigOption::intOption("Time length of a pulse (in µs).", 5, 1, ((0x01 << 20) - 1)));
			config.add("externalInput/GenerateInjectOnRisingEdge",
				dv::ConfigOption::boolOption("Emit a special event when a rising edge is generated."));
			config.add("externalInput/GenerateInjectOnFallingEdge",
				dv::ConfigOption::boolOption("Emit a special event when a falling edge is generated."));
		}
	}

	void externalInputConfigSend(const struct caer_dvx_info *devInfo) {
		device.configSet(
			DVX_EXTINPUT, DVX_EXTINPUT_DETECT_RISING_EDGES, config.getBool("externalInput/DetectRisingEdges"));
		device.configSet(
			DVX_EXTINPUT, DVX_EXTINPUT_DETECT_FALLING_EDGES, config.getBool("externalInput/DetectFallingEdges"));
		device.configSet(DVX_EXTINPUT, DVX_EXTINPUT_DETECT_PULSES, config.getBool("externalInput/DetectPulses"));
		device.configSet(
			DVX_EXTINPUT, DVX_EXTINPUT_DETECT_PULSE_POLARITY, config.getBool("externalInput/DetectPulsePolarity"));
		device.configSet(DVX_EXTINPUT, DVX_EXTINPUT_DETECT_PULSE_LENGTH,
			static_cast<uint32_t>(config.getInt("externalInput/DetectPulseLength")));
		device.configSet(DVX_EXTINPUT, DVX_EXTINPUT_RUN_DETECTOR, config.getBool("externalInput/RunDetector"));

		if (devInfo->extInputHasGenerator) {
			device.configSet(DVX_EXTINPUT, DVX_EXTINPUT_GENERATE_PULSE_POLARITY,
				config.getBool("externalInput/GeneratePulsePolarity"));
			device.configSet(DVX_EXTINPUT, DVX_EXTINPUT_GENERATE_PULSE_INTERVAL,
				static_cast<uint32_t>(config.getInt("externalInput/GeneratePulseInterval")));
			device.configSet(DVX_EXTINPUT, DVX_EXTINPUT_GENERATE_PULSE_LENGTH,
				static_cast<uint32_t>(config.getInt("externalInput/GeneratePulseLength")));
			device.configSet(DVX_EXTINPUT, DVX_EXTINPUT_GENERATE_INJECT_ON_RISING_EDGE,
				config.getBool("externalInput/GenerateInjectOnRisingEdge"));
			device.configSet(DVX_EXTINPUT, DVX_EXTINPUT_GENERATE_INJECT_ON_FALLING_EDGE,
				config.getBool("externalInput/GenerateInjectOnFallingEdge"));
			device.configSet(DVX_EXTINPUT, DVX_EXTINPUT_RUN_GENERATOR, config.getBool("externalInput/RunGenerator"));
		}
	}

	static void externalInputConfigListener(dvConfigNode node, void *userData, enum dvConfigAttributeEvents event,
		const char *changeKey, enum dvConfigAttributeType changeType, union dvConfigAttributeValue changeValue) {
		UNUSED_ARGUMENT(node);

		auto device = static_cast<libcaer::devices::dvXplorer *>(userData);

		std::string key{changeKey};

		if (event == DVCFG_ATTRIBUTE_MODIFIED) {
			if (changeType == DVCFG_TYPE_BOOL && key == "DetectRisingEdges") {
				device->configSet(DVX_EXTINPUT, DVX_EXTINPUT_DETECT_RISING_EDGES, changeValue.boolean);
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "DetectFallingEdges") {
				device->configSet(DVX_EXTINPUT, DVX_EXTINPUT_DETECT_FALLING_EDGES, changeValue.boolean);
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "DetectPulses") {
				device->configSet(DVX_EXTINPUT, DVX_EXTINPUT_DETECT_PULSES, changeValue.boolean);
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "DetectPulsePolarity") {
				device->configSet(DVX_EXTINPUT, DVX_EXTINPUT_DETECT_PULSE_POLARITY, changeValue.boolean);
			}
			else if (changeType == DVCFG_TYPE_INT && key == "DetectPulseLength") {
				device->configSet(
					DVX_EXTINPUT, DVX_EXTINPUT_DETECT_PULSE_LENGTH, static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "RunDetector") {
				device->configSet(DVX_EXTINPUT, DVX_EXTINPUT_RUN_DETECTOR, changeValue.boolean);
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "GeneratePulsePolarity") {
				device->configSet(DVX_EXTINPUT, DVX_EXTINPUT_GENERATE_PULSE_POLARITY, changeValue.boolean);
			}
			else if (changeType == DVCFG_TYPE_INT && key == "GeneratePulseInterval") {
				device->configSet(
					DVX_EXTINPUT, DVX_EXTINPUT_GENERATE_PULSE_INTERVAL, static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "GeneratePulseLength") {
				device->configSet(
					DVX_EXTINPUT, DVX_EXTINPUT_GENERATE_PULSE_LENGTH, static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "GenerateInjectOnRisingEdge") {
				device->configSet(DVX_EXTINPUT, DVX_EXTINPUT_GENERATE_INJECT_ON_RISING_EDGE, changeValue.boolean);
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "GenerateInjectOnFallingEdge") {
				device->configSet(DVX_EXTINPUT, DVX_EXTINPUT_GENERATE_INJECT_ON_FALLING_EDGE, changeValue.boolean);
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "RunGenerator") {
				device->configSet(DVX_EXTINPUT, DVX_EXTINPUT_RUN_GENERATOR, changeValue.boolean);
			}
		}
	}

	static void usbConfigCreate(dv::RuntimeConfig &config) {
		// Subsystem 9: FX2/3 USB Configuration and USB buffer settings.
		config.add("usb/EarlyPacketDelay",
			dv::ConfigOption::intOption(
				"Send early USB packets if this timeout is reached (in 125µs time-slices).", 8, 1, 8000));

		// USB buffer settings.
		config.add("usb/BufferNumber", dv::ConfigOption::intOption("Number of USB transfers.", 8, 2, 128));
		config.add("usb/BufferSize",
			dv::ConfigOption::intOption("Size in bytes of data buffers for USB transfers.", 8192, 512, 32768));

		config.setPriorityOptions({"usb/"});
	}

	void usbConfigSend() {
		device.configSet(CAER_HOST_CONFIG_USB, CAER_HOST_CONFIG_USB_BUFFER_NUMBER,
			static_cast<uint32_t>(config.getInt("usb/BufferNumber")));
		device.configSet(CAER_HOST_CONFIG_USB, CAER_HOST_CONFIG_USB_BUFFER_SIZE,
			static_cast<uint32_t>(config.getInt("usb/BufferSize")));

		device.configSet(
			DVX_USB, DVX_USB_EARLY_PACKET_DELAY, static_cast<uint32_t>(config.getInt("usb/EarlyPacketDelay")));
	}

	static void usbConfigListener(dvConfigNode node, void *userData, enum dvConfigAttributeEvents event,
		const char *changeKey, enum dvConfigAttributeType changeType, union dvConfigAttributeValue changeValue) {
		UNUSED_ARGUMENT(node);

		auto device = static_cast<libcaer::devices::dvXplorer *>(userData);

		std::string key{changeKey};

		if (event == DVCFG_ATTRIBUTE_MODIFIED) {
			if (changeType == DVCFG_TYPE_INT && key == "BufferNumber") {
				device->configSet(
					CAER_HOST_CONFIG_USB, CAER_HOST_CONFIG_USB_BUFFER_NUMBER, static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "BufferSize") {
				device->configSet(
					CAER_HOST_CONFIG_USB, CAER_HOST_CONFIG_USB_BUFFER_SIZE, static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "EarlyPacketDelay") {
				device->configSet(DVX_USB, DVX_USB_EARLY_PACKET_DELAY, static_cast<uint32_t>(changeValue.iint));
			}
		}
	}

	static void systemConfigCreate(dv::RuntimeConfig &config) {
		// Packet settings (size (in events) and time interval (in µs)).
		config.add("system/PacketContainerMaxPacketSize",
			dv::ConfigOption::intOption("Maximum packet size in events, when any packet reaches this size, the "
										"EventPacketContainer is sent for processing.",
				0, 0, 10 * 1024 * 1024));
		config.add("system/PacketContainerInterval",
			dv::ConfigOption::intOption("Time interval in µs, each sent EventPacketContainer will span this interval.",
				10000, 1, 120 * 1000 * 1000));

		// Ring-buffer setting (only changes value on module init/shutdown cycles).
		config.add("system/DataExchangeBufferSize",
			dv::ConfigOption::intOption(
				"Size of EventPacketContainer queue, used for transfers between data acquisition thread and mainloop.",
				64, 8, 1024));

		config.setPriorityOptions({"system/"});
	}

	void systemConfigSend() {
		device.configSet(CAER_HOST_CONFIG_PACKETS, CAER_HOST_CONFIG_PACKETS_MAX_CONTAINER_PACKET_SIZE,
			static_cast<uint32_t>(config.getInt("system/PacketContainerMaxPacketSize")));
		device.configSet(CAER_HOST_CONFIG_PACKETS, CAER_HOST_CONFIG_PACKETS_MAX_CONTAINER_INTERVAL,
			static_cast<uint32_t>(config.getInt("system/PacketContainerInterval")));

		// Changes only take effect on module start!
		device.configSet(CAER_HOST_CONFIG_DATAEXCHANGE, CAER_HOST_CONFIG_DATAEXCHANGE_BUFFER_SIZE,
			static_cast<uint32_t>(config.getInt("system/DataExchangeBufferSize")));
	}

	static void systemConfigListener(dvConfigNode node, void *userData, enum dvConfigAttributeEvents event,
		const char *changeKey, enum dvConfigAttributeType changeType, union dvConfigAttributeValue changeValue) {
		UNUSED_ARGUMENT(node);

		auto device = static_cast<libcaer::devices::dvXplorer *>(userData);

		std::string key{changeKey};
		if (event == DVCFG_ATTRIBUTE_MODIFIED) {
			if (changeType == DVCFG_TYPE_INT && key == "PacketContainerMaxPacketSize") {
				device->configSet(CAER_HOST_CONFIG_PACKETS, CAER_HOST_CONFIG_PACKETS_MAX_CONTAINER_PACKET_SIZE,
					static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "PacketContainerInterval") {
				device->configSet(CAER_HOST_CONFIG_PACKETS, CAER_HOST_CONFIG_PACKETS_MAX_CONTAINER_INTERVAL,
					static_cast<uint32_t>(changeValue.iint));
			}
		}
	}

	static union dvConfigAttributeValue statisticsUpdater(
		void *userData, const char *key, enum dvConfigAttributeType type) {
		UNUSED_ARGUMENT(type); // We know all statistics are always LONG.

		auto device = static_cast<libcaer::devices::dvXplorer *>(userData);

		std::string keyStr{key};

		union dvConfigAttributeValue statisticValue = {.ilong = 0};

		try {
			if (keyStr == "muxDroppedDVS") {
				device->configGet64(
					DVX_MUX, DVX_MUX_STATISTICS_DVS_DROPPED, reinterpret_cast<uint64_t *>(&statisticValue.ilong));
			}
			else if (keyStr == "muxDroppedExtInput") {
				device->configGet64(
					DVX_MUX, DVX_MUX_STATISTICS_EXTINPUT_DROPPED, reinterpret_cast<uint64_t *>(&statisticValue.ilong));
			}
			else if (keyStr == "dvsColumns") {
				device->configGet64(
					DVX_DVS, DVX_DVS_STATISTICS_COLUMN, reinterpret_cast<uint64_t *>(&statisticValue.ilong));
			}
			else if (keyStr == "dvsGroups") {
				device->configGet64(
					DVX_DVS, DVX_DVS_STATISTICS_GROUP, reinterpret_cast<uint64_t *>(&statisticValue.ilong));
			}
			else if (keyStr == "dvsDroppedColumns") {
				device->configGet64(
					DVX_DVS, DVX_DVS_STATISTICS_DROPPED_COLUMN, reinterpret_cast<uint64_t *>(&statisticValue.ilong));
			}
			else if (keyStr == "dvsDroppedGroups") {
				device->configGet64(
					DVX_DVS, DVX_DVS_STATISTICS_DROPPED_GROUP, reinterpret_cast<uint64_t *>(&statisticValue.ilong));
			}
		}
		catch (const std::runtime_error &) {
			// Catch communication failures and ignore them.
		}

		return (statisticValue);
	}

	static uint32_t mapSubsampleFactor(const std::string &strVal) {
		if (strVal == "1/2") {
			return (DVX_DVS_CHIP_SUBSAMPLE_HORIZONTAL_HALF);
		}
		else if (strVal == "1/4") {
			return (DVX_DVS_CHIP_SUBSAMPLE_HORIZONTAL_FOURTH);
		}
		else if (strVal == "1/8") {
			return (DVX_DVS_CHIP_SUBSAMPLE_HORIZONTAL_EIGHTH);
		}
		else {
			// No sub-sampling.
			return (DVX_DVS_CHIP_SUBSAMPLE_HORIZONTAL_NONE);
		}
	}

	static uint32_t mapAccelDataRate(const std::string &strVal) {
		if (strVal == "12.5 Hz") {
			return (BOSCH_ACCEL_12_5HZ);
		}
		else if (strVal == "25 Hz") {
			return (BOSCH_ACCEL_25HZ);
		}
		else if (strVal == "50 Hz") {
			return (BOSCH_ACCEL_50HZ);
		}
		else if (strVal == "100 Hz") {
			return (BOSCH_ACCEL_100HZ);
		}
		else if (strVal == "200 Hz") {
			return (BOSCH_ACCEL_200HZ);
		}
		else if (strVal == "400 Hz") {
			return (BOSCH_ACCEL_400HZ);
		}
		else if (strVal == "800 Hz") {
			return (BOSCH_ACCEL_800HZ);
		}
		else {
			return (BOSCH_ACCEL_1600HZ);
		}
	}

	static uint32_t mapAccelFilter(const std::string &strVal) {
		if (strVal == "Normal") {
			return (BOSCH_ACCEL_NORMAL);
		}
		else if (strVal == "OSR2") {
			return (BOSCH_ACCEL_OSR2);
		}
		else {
			return (BOSCH_ACCEL_OSR4);
		}
	}

	static uint32_t mapAccelRange(const std::string &strVal) {
		if (strVal == "±2G") {
			return (BOSCH_ACCEL_2G);
		}
		else if (strVal == "±4G") {
			return (BOSCH_ACCEL_4G);
		}
		else if (strVal == "±8G") {
			return (BOSCH_ACCEL_8G);
		}
		else {
			return (BOSCH_ACCEL_16G);
		}
	}

	static uint32_t mapGyroDataRate(const std::string &strVal) {
		if (strVal == "25 Hz") {
			return (BOSCH_GYRO_25HZ);
		}
		else if (strVal == "50 Hz") {
			return (BOSCH_GYRO_50HZ);
		}
		else if (strVal == "100 Hz") {
			return (BOSCH_GYRO_100HZ);
		}
		else if (strVal == "200 Hz") {
			return (BOSCH_GYRO_200HZ);
		}
		else if (strVal == "400 Hz") {
			return (BOSCH_GYRO_400HZ);
		}
		else if (strVal == "800 Hz") {
			return (BOSCH_GYRO_800HZ);
		}
		else if (strVal == "1600 Hz") {
			return (BOSCH_GYRO_1600HZ);
		}
		else {
			return (BOSCH_GYRO_3200HZ);
		}
	}

	static uint32_t mapGyroFilter(const std::string &strVal) {
		if (strVal == "Normal") {
			return (BOSCH_GYRO_NORMAL);
		}
		else if (strVal == "OSR2") {
			return (BOSCH_GYRO_OSR2);
		}
		else {
			return (BOSCH_GYRO_OSR4);
		}
	}

	static uint32_t mapGyroRange(const std::string &strVal) {
		if (strVal == "±125°/s") {
			return (BOSCH_GYRO_125DPS);
		}
		else if (strVal == "±250°/s") {
			return (BOSCH_GYRO_250DPS);
		}
		else if (strVal == "±500°/s") {
			return (BOSCH_GYRO_500DPS);
		}
		else if (strVal == "±1000°/s") {
			return (BOSCH_GYRO_1000DPS);
		}
		else {
			return (BOSCH_GYRO_2000DPS);
		}
	}
};

registerModuleClass(dvXplorer)
