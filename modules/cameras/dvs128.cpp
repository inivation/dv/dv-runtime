#include "../../include/dv-sdk/data/event.hpp"
#include "../../include/dv-sdk/data/trigger.hpp"
#include "../../include/dv-sdk/module.hpp"

#include "../../src/log.hpp"
#include "aedat4_convert.hpp"

#include <libcaercpp/devices/device_discover.hpp>
#include <libcaercpp/devices/dvs128.hpp>

#include <chrono>

void *dvModuleGetHooks(enum dvModuleHooks hook) {
	// Support only device discovery.
	if (hook != DV_HOOK_DEVICE_DISCOVERY) {
		return nullptr;
	}

	// Get current device status via libcaer.
	// Suppress logging of libusb errors.
	std::vector<struct caer_device_discovery_result> devices;

	try {
		libcaer::log::disable(true);

		devices = libcaer::devices::discover::device(CAER_DEVICE_DVS128);

		libcaer::log::disable(false);
	}
	catch (const std::exception &ex) {
		dv::Log(dv::logLevel::ERROR, "Device Discovery (DVS128): '{:s} :: {:s}'.",
			boost::core::demangle(typeid(ex).name()), ex.what());
		return nullptr;
	}

	// Add devices to config tree.
	auto devicesNode = dv::Cfg::GLOBAL.getNode("/system/_devices/");

	for (const auto &dev : devices) {
		const struct caer_dvs128_info *info = &dev.deviceInfo.dvs128Info;

		const auto nodeName = fmt::format(FMT_STRING("dvs128_{:d}-{:d}/"), static_cast<int>(info->deviceUSBBusNumber),
			static_cast<int>(info->deviceUSBDeviceAddress));

		auto devNode = devicesNode.getRelativeNode(nodeName);

		devNode.create<dv::CfgType::STRING>("OpenWithModule", "dv_dvs128", {1, 32},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Open device with specified module.");

		devNode.create<dv::CfgType::INT>("USBBusNumber", info->deviceUSBBusNumber, {0, 255},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "USB bus number.");
		devNode.create<dv::CfgType::INT>("USBDeviceAddress", info->deviceUSBDeviceAddress, {0, 255},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "USB device address.");

		if (!dev.deviceErrorOpen) {
			devNode.create<dv::CfgType::STRING>("SerialNumber", info->deviceSerialNumber, {0, 8},
				dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "USB device serial number.");
			devNode.create<dv::CfgType::INT>("FirmwareVersion", info->firmwareVersion, {0, INT16_MAX},
				dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Version of device firmware.");

			if (!dev.deviceErrorVersion) {
				devNode.create<dv::CfgType::BOOL>("DeviceIsMaster", info->deviceIsMaster, {},
					dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Device is timestamp master.");

				devNode.create<dv::CfgType::INT>("DVSSizeX", info->dvsSizeX, {0, INT16_MAX},
					dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "DVS X axis resolution.");
				devNode.create<dv::CfgType::INT>("DVSSizeY", info->dvsSizeY, {0, INT16_MAX},
					dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "DVS Y axis resolution.");
			}
		}
	}

	// Nothing to return, no further steps needed.
	return nullptr;
}

class dvs128 : public dv::ModuleBase {
private:
	libcaer::devices::dvs128 device;

public:
	static void initOutputs(dv::OutputDefinitionList &out) {
		out.addEventOutput("events");
		out.addTriggerOutput("triggers");
	}

	static const char *initDescription() {
		return ("iniVation DVS128 camera support.");
	}

	static void initConfigOptions(dv::RuntimeConfig &config) {
		config.add("busNumber", dv::ConfigOption::intOption("USB bus number restriction.", 0, 0, UINT8_MAX));
		config.add("devAddress", dv::ConfigOption::intOption("USB device address restriction.", 0, 0, UINT8_MAX));
		config.add("serialNumber", dv::ConfigOption::stringOption("USB serial number restriction.", ""));

		biasConfigCreate(config);
		dvsConfigCreate(config);
		usbConfigCreate(config);
		systemConfigCreate(config);
	}

	dvs128() :
		device(0, static_cast<uint8_t>(config.getInt("busNumber")), static_cast<uint8_t>(config.getInt("devAddress")),
			config.getString("serialNumber")) {
		// Initialize per-device log-level to module log-level.
		config.setString("logLevel", "WARNING");
		device.configSet(CAER_HOST_CONFIG_LOG, CAER_HOST_CONFIG_LOG_LEVEL,
			static_cast<uint32_t>(dv::LoggerInternal::logLevelNameToInteger(config.getString("logLevel"))));

		auto devInfo = device.infoGet();

		// Generate source string for output modules.
		auto sourceString = std::string("DVS128_") + devInfo.deviceSerialNumber;

		// Setup outputs.
		outputs.getEventOutput("events").setup(device.infoGet().dvsSizeX, device.infoGet().dvsSizeY, sourceString);
		outputs.getTriggerOutput("triggers").setup(sourceString);

		auto sourceInfoNode = moduleNode.getRelativeNode("sourceInfo/");

		sourceInfoNode.create<dv::CfgType::STRING>("serialNumber", devInfo.deviceSerialNumber, {0, 8},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Device serial number.");
		sourceInfoNode.create<dv::CfgType::INT>("usbBusNumber", devInfo.deviceUSBBusNumber, {0, 255},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Device USB bus number.");
		sourceInfoNode.create<dv::CfgType::INT>("usbDeviceAddress", devInfo.deviceUSBDeviceAddress, {0, 255},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Device USB device address.");

		sourceInfoNode.create<dv::CfgType::INT>("firmwareVersion", devInfo.firmwareVersion,
			{devInfo.firmwareVersion, devInfo.firmwareVersion}, dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT,
			"Device USB firmware version.");

		sourceInfoNode.create<dv::CfgType::BOOL>("deviceIsMaster", devInfo.deviceIsMaster, {},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT,
			"Timestamp synchronization support: device master status.");

		sourceInfoNode.create<dv::CfgType::STRING>("source", sourceString,
			{static_cast<int32_t>(sourceString.length()), static_cast<int32_t>(sourceString.length())},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Device source information.");

		// Ensure good defaults for data acquisition settings.
		// No blocking behavior due to mainloop notification, and no auto-start of
		// all producers to ensure cAER settings are respected.
		device.configSet(CAER_HOST_CONFIG_DATAEXCHANGE, CAER_HOST_CONFIG_DATAEXCHANGE_BLOCKING, true);
		device.configSet(CAER_HOST_CONFIG_DATAEXCHANGE, CAER_HOST_CONFIG_DATAEXCHANGE_START_PRODUCERS, false);
		device.configSet(CAER_HOST_CONFIG_DATAEXCHANGE, CAER_HOST_CONFIG_DATAEXCHANGE_STOP_PRODUCERS, true);

		// Create default settings and send them to the device.
		biasConfigSend();
		systemConfigSend();
		usbConfigSend();
		dvsConfigSend();

		// Set timestamp offset for real-time timestamps. DataStart() will
		// reset the device-side timestamp.
		int64_t tsNowOffset
			= std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch())
				  .count();

		sourceInfoNode.create<dv::CfgType::LONG>("tsOffset", tsNowOffset, {0, INT64_MAX},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT,
			"Time offset of data stream starting point to Unix time in µs.");

		moduleNode.getRelativeNode("outputs/events/info/")
			.create<dv::CfgType::LONG>("tsOffset", tsNowOffset, {0, INT64_MAX},
				dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT,
				"Time offset of data stream starting point to Unix time in µs.");

		moduleNode.getRelativeNode("outputs/triggers/info/")
			.create<dv::CfgType::LONG>("tsOffset", tsNowOffset, {0, INT64_MAX},
				dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT,
				"Time offset of data stream starting point to Unix time in µs.");

		// Start data acquisition.
		device.dataStart(nullptr, nullptr, nullptr, &moduleShutdownNotify, moduleData->moduleNode);

		// Add config listeners last, to avoid having them dangling if Init doesn't succeed.
		moduleNode.getRelativeNode("bias/").addAttributeListener(&device, &biasConfigListener);

		moduleNode.getRelativeNode("dvs/").addAttributeListener(&device, &dvsConfigListener);

		moduleNode.getRelativeNode("usb/").addAttributeListener(&device, &usbConfigListener);

		moduleNode.getRelativeNode("system/").addAttributeListener(&device, &systemConfigListener);

		moduleNode.addAttributeListener(&device, &logLevelListener);
	}

	~dvs128() override {
		// Remove listener, which can reference invalid memory in userData.
		moduleNode.getRelativeNode("bias/").removeAttributeListener(&device, &biasConfigListener);

		moduleNode.getRelativeNode("dvs/").removeAttributeListener(&device, &dvsConfigListener);

		moduleNode.getRelativeNode("usb/").removeAttributeListener(&device, &usbConfigListener);

		moduleNode.getRelativeNode("system/").removeAttributeListener(&device, &systemConfigListener);

		moduleNode.removeAttributeListener(&device, &logLevelListener);

		// Stop data acquisition.
		device.dataStop();

		// Clear sourceInfo node.
		auto sourceInfoNode = moduleNode.getRelativeNode("sourceInfo/");
		sourceInfoNode.removeAllAttributes();
	}

	void run() override {
		auto data = device.dataGet();

		if (!data || data->empty()) {
			return;
		}

		if (data->getEventPacket(SPECIAL_EVENT)) {
			std::shared_ptr<const libcaer::events::SpecialEventPacket> special
				= std::static_pointer_cast<libcaer::events::SpecialEventPacket>(data->getEventPacket(SPECIAL_EVENT));

			if (special->getEventNumber() == 1 && (*special)[0].getType() == TIMESTAMP_RESET) {
				// Update master/slave information.
				auto devInfo = device.infoGet();

				auto sourceInfoNode = moduleNode.getRelativeNode("sourceInfo/");
				sourceInfoNode.updateReadOnly<dv::CfgType::BOOL>("deviceIsMaster", devInfo.deviceIsMaster);

				// Reset real-time timestamp offset.
				int64_t tsNowOffset = std::chrono::duration_cast<std::chrono::microseconds>(
					std::chrono::system_clock::now().time_since_epoch())
										  .count();

				sourceInfoNode.updateReadOnly<dv::CfgType::LONG>("tsOffset", tsNowOffset);

				moduleNode.getRelativeNode("outputs/events/info/")
					.updateReadOnly<dv::CfgType::LONG>("tsOffset", tsNowOffset);

				moduleNode.getRelativeNode("outputs/triggers/info/")
					.updateReadOnly<dv::CfgType::LONG>("tsOffset", tsNowOffset);
			}

			dvConvertToAedat4(special->getHeaderPointer(), moduleData);
		}

		if (data->size() == 1) {
			return;
		}

		if (data->getEventPacket(POLARITY_EVENT)) {
			dvConvertToAedat4(data->getEventPacket(POLARITY_EVENT)->getHeaderPointer(), moduleData);
		}
	}

private:
	static void moduleShutdownNotify(void *p) {
		dv::Cfg::Node moduleNode = static_cast<dvConfigNode>(p);

		// Ensure parent also shuts down (on disconnected device for example).
		moduleNode.putBool("running", false);
	}

	static void biasConfigCreate(dv::RuntimeConfig &config) {
		// Bias settings.
		config.add("bias/cas", dv::ConfigOption::intOption("Photoreceptor cascode.", 1992, 0, (0x01 << 24) - 1));
		config.add(
			"bias/injGnd", dv::ConfigOption::intOption("Differentiator switch level.", 1108364, 0, (0x01 << 24) - 1));
		config.add("bias/reqPd", dv::ConfigOption::intOption("AER request pull-down.", 16777215, 0, (0x01 << 24) - 1));
		config.add(
			"bias/puX", dv::ConfigOption::intOption("2nd dimension AER static pull-up.", 8159221, 0, (0x01 << 24) - 1));
		config.add("bias/diffOff",
			dv::ConfigOption::intOption("OFF threshold - lower to raise threshold.", 132, 0, (0x01 << 24) - 1));
		config.add("bias/req", dv::ConfigOption::intOption("OFF request inverter bias.", 309590, 0, (0x01 << 24) - 1));
		config.add("bias/refr", dv::ConfigOption::intOption("Refractory period.", 969, 0, (0x01 << 24) - 1));
		config.add("bias/puY",
			dv::ConfigOption::intOption("1st dimension AER static pull-up.", 16777215, 0, (0x01 << 24) - 1));
		config.add("bias/diffOn",
			dv::ConfigOption::intOption("ON threshold - higher to raise threshold.", 209996, 0, (0x01 << 24) - 1));
		config.add("bias/diff", dv::ConfigOption::intOption("Differentiator.", 13125, 0, (0x01 << 24) - 1));
		config.add(
			"bias/foll", dv::ConfigOption::intOption("Source follower buffer between photoreceptor and differentiator.",
							 271, 0, (0x01 << 24) - 1));
		config.add("bias/pr", dv::ConfigOption::intOption("Photoreceptor.", 217, 0, (0x01 << 24) - 1));

		config.setPriorityOptions({"bias/diffOn", "bias/diffOff"});
	}

	void biasConfigSend() {
		device.configSet(DVS128_CONFIG_BIAS, DVS128_CONFIG_BIAS_CAS, static_cast<uint32_t>(config.getInt("bias/cas")));
		device.configSet(
			DVS128_CONFIG_BIAS, DVS128_CONFIG_BIAS_INJGND, static_cast<uint32_t>(config.getInt("bias/injGnd")));
		device.configSet(
			DVS128_CONFIG_BIAS, DVS128_CONFIG_BIAS_REQPD, static_cast<uint32_t>(config.getInt("bias/reqPd")));
		device.configSet(DVS128_CONFIG_BIAS, DVS128_CONFIG_BIAS_PUX, static_cast<uint32_t>(config.getInt("bias/puX")));
		device.configSet(
			DVS128_CONFIG_BIAS, DVS128_CONFIG_BIAS_DIFFOFF, static_cast<uint32_t>(config.getInt("bias/diffOff")));
		device.configSet(DVS128_CONFIG_BIAS, DVS128_CONFIG_BIAS_REQ, static_cast<uint32_t>(config.getInt("bias/req")));
		device.configSet(
			DVS128_CONFIG_BIAS, DVS128_CONFIG_BIAS_REFR, static_cast<uint32_t>(config.getInt("bias/refr")));
		device.configSet(DVS128_CONFIG_BIAS, DVS128_CONFIG_BIAS_PUY, static_cast<uint32_t>(config.getInt("bias/puY")));
		device.configSet(
			DVS128_CONFIG_BIAS, DVS128_CONFIG_BIAS_DIFFON, static_cast<uint32_t>(config.getInt("bias/diffOn")));
		device.configSet(
			DVS128_CONFIG_BIAS, DVS128_CONFIG_BIAS_DIFF, static_cast<uint32_t>(config.getInt("bias/diff")));
		device.configSet(
			DVS128_CONFIG_BIAS, DVS128_CONFIG_BIAS_FOLL, static_cast<uint32_t>(config.getInt("bias/foll")));
		device.configSet(DVS128_CONFIG_BIAS, DVS128_CONFIG_BIAS_PR, static_cast<uint32_t>(config.getInt("bias/pr")));
	}

	static void biasConfigListener(dvConfigNode node, void *userData, enum dvConfigAttributeEvents event,
		const char *changeKey, enum dvConfigAttributeType changeType, union dvConfigAttributeValue changeValue) {
		UNUSED_ARGUMENT(node);

		auto device = static_cast<libcaer::devices::dvs128 *>(userData);

		std::string key{changeKey};

		if (event == DVCFG_ATTRIBUTE_MODIFIED) {
			if (changeType == DVCFG_TYPE_INT && key == "cas") {
				device->configSet(DVS128_CONFIG_BIAS, DVS128_CONFIG_BIAS_CAS, static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "injGnd") {
				device->configSet(
					DVS128_CONFIG_BIAS, DVS128_CONFIG_BIAS_INJGND, static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "reqPd") {
				device->configSet(
					DVS128_CONFIG_BIAS, DVS128_CONFIG_BIAS_REQPD, static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "puX") {
				device->configSet(DVS128_CONFIG_BIAS, DVS128_CONFIG_BIAS_PUX, static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "diffOff") {
				device->configSet(
					DVS128_CONFIG_BIAS, DVS128_CONFIG_BIAS_DIFFOFF, static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "req") {
				device->configSet(DVS128_CONFIG_BIAS, DVS128_CONFIG_BIAS_REQ, static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "refr") {
				device->configSet(DVS128_CONFIG_BIAS, DVS128_CONFIG_BIAS_REFR, static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "puY") {
				device->configSet(DVS128_CONFIG_BIAS, DVS128_CONFIG_BIAS_PUY, static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "diffOn") {
				device->configSet(
					DVS128_CONFIG_BIAS, DVS128_CONFIG_BIAS_DIFFON, static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "diff") {
				device->configSet(DVS128_CONFIG_BIAS, DVS128_CONFIG_BIAS_DIFF, static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "foll") {
				device->configSet(DVS128_CONFIG_BIAS, DVS128_CONFIG_BIAS_FOLL, static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "pr") {
				device->configSet(DVS128_CONFIG_BIAS, DVS128_CONFIG_BIAS_PR, static_cast<uint32_t>(changeValue.iint));
			}
		}
	}

	static void dvsConfigCreate(dv::RuntimeConfig &config) {
		// DVS settings.
		config.add("dvs/Run", dv::ConfigOption::boolOption("Run DVS to get polarity events.", true));
		config.add(
			"dvs/TimestampReset", dv::ConfigOption::buttonOption("Reset timestamps to zero.", "Reset timestamps"));
		config.add("dvs/ArrayReset", dv::ConfigOption::buttonOption("Reset DVS pixel array.", "Reset DVS pixels"));

		config.setPriorityOptions({"dvs/"});
	}

	void dvsConfigSend() {
		device.configSet(DVS128_CONFIG_DVS, DVS128_CONFIG_DVS_ARRAY_RESET, config.getBool("dvs/ArrayReset"));
		config.setBool("dvs/ArrayReset", false); // Ensure default value is reset.
		device.configSet(DVS128_CONFIG_DVS, DVS128_CONFIG_DVS_TIMESTAMP_RESET, config.getBool("dvs/TimestampReset"));
		config.setBool("dvs/TimestampReset", false); // Ensure default value is reset.
		device.configSet(DVS128_CONFIG_DVS, DVS128_CONFIG_DVS_RUN, config.getBool("dvs/Run"));
	}

	static void dvsConfigListener(dvConfigNode node, void *userData, enum dvConfigAttributeEvents event,
		const char *changeKey, enum dvConfigAttributeType changeType, union dvConfigAttributeValue changeValue) {
		auto device = static_cast<libcaer::devices::dvs128 *>(userData);

		std::string key{changeKey};

		if (event == DVCFG_ATTRIBUTE_MODIFIED) {
			if (changeType == DVCFG_TYPE_BOOL && key == "ArrayReset" && changeValue.boolean) {
				device->configSet(DVS128_CONFIG_DVS, DVS128_CONFIG_DVS_ARRAY_RESET, changeValue.boolean);

				dvConfigNodeAttributeBooleanReset(node, changeKey);
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "TimestampReset" && changeValue.boolean) {
				device->configSet(DVS128_CONFIG_DVS, DVS128_CONFIG_DVS_TIMESTAMP_RESET, changeValue.boolean);

				dvConfigNodeAttributeBooleanReset(node, changeKey);
			}
			else if (changeType == DVCFG_TYPE_BOOL && key == "Run") {
				device->configSet(DVS128_CONFIG_DVS, DVS128_CONFIG_DVS_RUN, changeValue.boolean);
			}
		}
	}

	static void usbConfigCreate(dv::RuntimeConfig &config) {
		// USB buffer settings.
		config.add("usb/BufferNumber", dv::ConfigOption::intOption("Number of USB transfers.", 8, 2, 128));
		config.add("usb/BufferSize",
			dv::ConfigOption::intOption("Size in bytes of data buffers for USB transfers.", 4096, 512, 32768));

		config.setPriorityOptions({"usb/"});
	}

	void usbConfigSend() {
		device.configSet(CAER_HOST_CONFIG_USB, CAER_HOST_CONFIG_USB_BUFFER_NUMBER,
			static_cast<uint32_t>(config.getInt("usb/BufferNumber")));
		device.configSet(CAER_HOST_CONFIG_USB, CAER_HOST_CONFIG_USB_BUFFER_SIZE,
			static_cast<uint32_t>(config.getInt("usb/BufferSize")));
	}

	static void usbConfigListener(dvConfigNode node, void *userData, enum dvConfigAttributeEvents event,
		const char *changeKey, enum dvConfigAttributeType changeType, union dvConfigAttributeValue changeValue) {
		UNUSED_ARGUMENT(node);

		auto device = static_cast<libcaer::devices::dvs128 *>(userData);

		std::string key{changeKey};

		if (event == DVCFG_ATTRIBUTE_MODIFIED) {
			if (changeType == DVCFG_TYPE_INT && key == "BufferNumber") {
				device->configSet(
					CAER_HOST_CONFIG_USB, CAER_HOST_CONFIG_USB_BUFFER_NUMBER, static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "BufferSize") {
				device->configSet(
					CAER_HOST_CONFIG_USB, CAER_HOST_CONFIG_USB_BUFFER_SIZE, static_cast<uint32_t>(changeValue.iint));
			}
		}
	}

	static void systemConfigCreate(dv::RuntimeConfig &config) {
		// Packet settings (size (in events) and time interval (in µs)).
		config.add("system/PacketContainerMaxPacketSize",
			dv::ConfigOption::intOption("Maximum packet size in events, when any packet reaches this size, the "
										"EventPacketContainer is sent for processing.",
				0, 0, 10 * 1024 * 1024));
		config.add("system/PacketContainerInterval",
			dv::ConfigOption::intOption("Time interval in µs, each sent EventPacketContainer will span this interval.",
				10000, 1, 120 * 1000 * 1000));

		// Ring-buffer setting (only changes value on module init/shutdown cycles).
		config.add("system/DataExchangeBufferSize",
			dv::ConfigOption::intOption(
				"Size of EventPacketContainer queue, used for transfers between data acquisition thread and mainloop.",
				64, 8, 1024));

		config.setPriorityOptions({"system/"});
	}

	void systemConfigSend() {
		device.configSet(CAER_HOST_CONFIG_PACKETS, CAER_HOST_CONFIG_PACKETS_MAX_CONTAINER_PACKET_SIZE,
			static_cast<uint32_t>(config.getInt("system/PacketContainerMaxPacketSize")));
		device.configSet(CAER_HOST_CONFIG_PACKETS, CAER_HOST_CONFIG_PACKETS_MAX_CONTAINER_INTERVAL,
			static_cast<uint32_t>(config.getInt("system/PacketContainerInterval")));

		// Changes only take effect on module start!
		device.configSet(CAER_HOST_CONFIG_DATAEXCHANGE, CAER_HOST_CONFIG_DATAEXCHANGE_BUFFER_SIZE,
			static_cast<uint32_t>(config.getInt("system/DataExchangeBufferSize")));
	}

	static void systemConfigListener(dvConfigNode node, void *userData, enum dvConfigAttributeEvents event,
		const char *changeKey, enum dvConfigAttributeType changeType, union dvConfigAttributeValue changeValue) {
		UNUSED_ARGUMENT(node);

		auto device = static_cast<libcaer::devices::dvs128 *>(userData);

		std::string key{changeKey};

		if (event == DVCFG_ATTRIBUTE_MODIFIED) {
			if (changeType == DVCFG_TYPE_INT && key == "PacketContainerMaxPacketSize") {
				device->configSet(CAER_HOST_CONFIG_PACKETS, CAER_HOST_CONFIG_PACKETS_MAX_CONTAINER_PACKET_SIZE,
					static_cast<uint32_t>(changeValue.iint));
			}
			else if (changeType == DVCFG_TYPE_INT && key == "PacketContainerInterval") {
				device->configSet(CAER_HOST_CONFIG_PACKETS, CAER_HOST_CONFIG_PACKETS_MAX_CONTAINER_INTERVAL,
					static_cast<uint32_t>(changeValue.iint));
			}
		}
	}

	static void logLevelListener(dvConfigNode node, void *userData, enum dvConfigAttributeEvents event,
		const char *changeKey, enum dvConfigAttributeType changeType, union dvConfigAttributeValue changeValue) {
		UNUSED_ARGUMENT(node);

		auto device = static_cast<libcaer::devices::dvs128 *>(userData);

		std::string key{changeKey};

		if (event == DVCFG_ATTRIBUTE_MODIFIED && changeType == DVCFG_TYPE_STRING && key == "logLevel") {
			device->configSet(CAER_HOST_CONFIG_LOG, CAER_HOST_CONFIG_LOG_LEVEL,
				static_cast<uint32_t>(dv::LoggerInternal::logLevelNameToInteger(changeValue.string)));
		}
	}
};

registerModuleClass(dvs128)
