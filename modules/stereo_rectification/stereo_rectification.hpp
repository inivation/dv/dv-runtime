#ifndef STEREO_RECTIFICATION_HPP
#define STEREO_RECTIFICATION_HPP

#include "../../include/dv-sdk/module.hpp"

#include <opencv2/core.hpp>

#include <regex>

class StereoRectification : public dv::ModuleBase {
public:
	static void initConfigOptions(dv::RuntimeConfig &config);

	StereoRectification();

	void configUpdate() override;

protected:
	static constexpr size_t NUM_CAMERAS = 2;

	struct CameraInfo {
		cv::Mat cameraMatrix;
		cv::Mat distCoeffs;
		int width;
		int height;
	};

	std::string id[NUM_CAMERAS];
	cv::Size size[NUM_CAMERAS];

	bool loadStereoRectificationMatrices(const std::string &filename);
	virtual void initStereoRectification(
		const CameraInfo loaded[NUM_CAMERAS], const cv::Mat R, const cv::Mat T, const double alpha)
		= 0;

private:
	static inline const std::regex FILENAME_CLEANUP_REGEX{"[^a-zA-Z-_\\d]"};

	static bool cvExists(const cv::FileNode &fn);
	static std::string getCameraID(const std::string &originDescription);
};

#endif // STEREO_RECTIFICATION_HPP
